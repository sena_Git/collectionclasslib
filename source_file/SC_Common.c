//
//  SC_Common.c
//  SenaClass
//
//  Created by matsushita sena on 2019/01/20.
//  Copyright © 2019年 matsushita sena. All rights reserved.
//

#include "SC_Common.h"

extern void SC_Pool_OwnerInit( void );

//if you use SC, you must call this.
void SC_Initializer( void )
{
    SC_Pool_OwnerInit();
}

//if you finished using SC, you must call this.
void SC_Finalizer( void )
{
    
}