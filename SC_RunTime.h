#ifndef SC_RunTime_h
#define SC_RunTime_h

#include "SC_Type.h"

struct SC_LogBuffer{
    char*   __Log;
    size_t  __LogSize;
    size_t  __memBlockSize;
};

struct SC_RunTime{
    SC_TypeID           __typeId;
    SC_UInt32           __retain;
    SC_UInt32           __release;
    
    pthread_mutex_t     __locker;
    
    SC_Object (* __CloneObj)( SC_Object );
    void (* __FreeObj)( SC_Object );
    void (* __LogInfo)( SC_Object, struct SC_LogBuffer* );
    void (* __ToJson)( SC_Object, SC_Object );
};

#endif /* SC_RunTime_h */


