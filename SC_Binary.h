//
//  SC_Binary.h
//  SenaClass
//
//  Created by matsushita sena on 2018/09/02.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#ifndef SC_Binary_h
#define SC_Binary_h

#include "SC_Type.h"

SC_Object SC_Binary_AllocInit();
SC_Object SC_Binary_AllocInitAutorelease();

void SC_Binary_AppendWrite( SC_Object obj, SC_Index from, const void* Binary, size_t byte );

void SC_Binary_Read( SC_Object obj, SC_Index from, void* Output, size_t byte );

size_t SC_Binary_Size( SC_Object obj, size_t OneSize );

void* SC_Binary_GetNative( SC_Object obj );
void SC_Binary_NativeLock( SC_Object obj );         //you can lock access from SC_Binary function
void SC_Binary_NativeUnlock( SC_Object obj );


#endif /* SC_Binary_h */
