//
//  SC_Binary.c
//  SenaClass
//
//  Created by matsushita sena on 2018/09/02.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#include "SC_Binary.h"
#include "SC_RunTime.h"

struct SC_Binary {
    struct SC_RunTime       __runtime;
    
    pthread_mutex_t         __locker;

    size_t                  __binarySize;
    size_t                  __memBlockCount;
    size_t                  __memOneBlockSize;
    void*                   __Binary;
};


#ifdef SC_TYPECHECK
    static void SC_Binary_TypeChecker( SC_TypeID* objId, const char* FuncName );
    #define __Binary_TypeChecker( obj ) (SC_Binary_TypeChecker( (SC_TypeID*)obj, __func__ ))
#else 
    #define __Binary_TypeChecker( ... )
#endif

static void SC_Binary_SetBlock( SC_Object obj, size_t SetBlockCount );


//---------------------------------NoLockFunc---------------------------------------------
SC_Object SC_Binary_AllocInit_NL();
void SC_Binary_AppendWrite_NL( SC_Object obj, SC_Index from, const void* Binary, size_t byte );
void SC_Binary_Read_NL( SC_Object obj, SC_Index from, void* Output, size_t byte );  
size_t SC_Binary_Size_NL( SC_Object obj, size_t OneSize );
void* SC_Binary_GetNative_NL( SC_Object obj );
void SC_Binary_NativeLock_NL( SC_Object obj );
void SC_Binary_NativeUnlock( SC_Object obj );

//-------------------------------MutexLocker---------------------------------------------
static inline void SC_Binary_LockerInit( struct SC_Binary* obj );
static inline void SC_Binary_Lock( struct SC_Binary* obj );
static inline void SC_Binary_UnLock( struct SC_Binary* obj );

//--------------------------------CallBackFuncs-------------------------------------------
void SC_Binary_freeObj( SC_Object obj );
SC_Object SC_Binary_CloneObj( SC_Object obj );
void SC_Binary_ToJson( SC_Object obj, SC_Object JsonBuffer );
void SC_Binary_LogInfo( SC_Object obj, struct SC_LogBuffer* LogBuffer );
//-----LogInfo----
extern void SC_LogBuffer_AddData( struct SC_LogBuffer* LogBuffer, const char* AddLog );

//--------------------------------RunTime_Alloc-------------------------------------------
extern void* SC_RunTime_AllocInit( SC_TypeID typeId, size_t obj_size );

//--------------------------------String_Alloc---------------------------------------------
extern SC_Object SC_String_AllocInitFormat( const char* format, ... );
extern void SC_String_InsertFormat( SC_Object obj, SC_SInt32 index, const char* format, ... );

//-------------------------------PoolAutoRelease------------------------------------------
extern SC_Object SC_Pool_Autorelease( SC_Object SetObj );



SC_Object SC_Binary_AllocInit()
{
    SC_Object obj = SC_Binary_AllocInit_NL();
    SC_Binary_LockerInit( obj );
    return obj;
}

SC_Object SC_Binary_AllocInitAutorelease()
{
    return SC_Pool_Autorelease( SC_Binary_AllocInit() );
}

SC_Object SC_Binary_AllocInit_NL()
{    
    struct SC_Binary* _obj = SC_RunTime_AllocInit( TypeId_Binary, sizeof( struct SC_Binary ) );
    _obj->__runtime.__ToJson = SC_Binary_ToJson;
    _obj->__runtime.__CloneObj = SC_Binary_CloneObj;
    _obj->__runtime.__LogInfo = SC_Binary_LogInfo;
    _obj->__runtime.__FreeObj = SC_Binary_freeObj;
    _obj->__binarySize = 0;
    _obj->__memOneBlockSize = SC_MALLOCSIZE/sizeof( char );
    _obj->__memBlockCount  = 1;
    
    SC_Binary_SetBlock( _obj, _obj->__memOneBlockSize*_obj->__memBlockCount );    
    
    return _obj;
}



void SC_Binary_AppendWrite( SC_Object obj, SC_Index from, const void* Binary, size_t byte )
{
    __Binary_TypeChecker( obj );
    
    SC_Binary_Lock( obj );
    SC_Binary_AppendWrite_NL( obj, from, Binary, byte );
    SC_Binary_UnLock( obj );
}

void SC_Binary_AppendWrite_NL( SC_Object obj, SC_Index from, const void* Binary, size_t byte )
{
    struct SC_Binary* _obj = (struct SC_Binary*)obj;
    
    if( (_obj->__memBlockCount*_obj->__memOneBlockSize) < (_obj->__binarySize+byte) )
    {
        SC_Binary_SetBlock( _obj, ((_obj->__binarySize+byte)/_obj->__memOneBlockSize)+1 );
    }
    
    memmove( &((char*)_obj->__Binary)[from+byte], &_obj->__Binary[from], _obj->__binarySize-(from+1) );
    memcpy( &((char*)_obj->__Binary)[from], Binary, byte );
    
    _obj->__binarySize += byte;
}



void SC_Binary_Read( SC_Object obj, SC_Index from, void* Output, size_t byte )
{
    __Binary_TypeChecker( obj );
    
    SC_Binary_Lock( obj );
    SC_Binary_Read_NL( obj, from, Output, byte );
    SC_Binary_UnLock( obj );
}

void SC_Binary_Read_NL( SC_Object obj, SC_Index from, void* Output, size_t byte )
{
    struct SC_Binary* _obj = (struct SC_Binary*)obj;
    
    memcpy( Output,  &((char*)_obj->__Binary)[from], byte );
}



size_t SC_Binary_Size( SC_Object obj, size_t OneSize )
{
    __Binary_TypeChecker( obj );
    
    SC_Binary_Lock( obj );
    size_t BinarySize = SC_Binary_Size_NL( obj, OneSize );
    SC_Binary_UnLock( obj );
    
    return BinarySize;
}
size_t SC_Binary_Size_NL( SC_Object obj, size_t OneSize )
{
    struct SC_Binary* _obj = (struct SC_Binary*)obj;
    
    return (size_t)(_obj->__binarySize/OneSize);
}



void* SC_Binary_GetNative( SC_Object obj )
{
    __Binary_TypeChecker( obj );
    
    SC_Binary_Lock( obj );
    void* Binary = SC_Binary_GetNative_NL( obj );
    SC_Binary_UnLock( obj );

    return Binary;
}

void* SC_Binary_GetNative_NL( SC_Object obj )
{
    struct SC_Binary* _obj = (struct SC_Binary*)obj;
	
    return _obj->__Binary;
}



void SC_Binary_NativeLock( SC_Object obj )
{
    __Binary_TypeChecker( obj );
    
    SC_Binary_Lock( obj );
}

void SC_Binary_NativeUnlock( SC_Object obj )
{
    __Binary_TypeChecker( obj );
    
    SC_Binary_UnLock( obj );
}



//-----------------------------------private-----------------------------------------------
#ifdef SC_TYPECHECK
static void SC_Binary_TypeChecker( SC_TypeID* objId, const char* FuncName )
{
    if( objId == NULL )
    {
        __DebugTypeEr( FuncName );
    }
    
    if( *objId != TypeId_Binary )
    {
        __DebugTypeEr( FuncName );
    }
}
#endif

static void SC_Binary_SetBlock( SC_Object obj, size_t SetBlockCount )
{
    struct SC_Binary* _obj = (struct SC_Binary*)obj;

    void* BinaryNew = realloc( _obj->__Binary, _obj->__memOneBlockSize*SetBlockCount );
    if( BinaryNew == NULL )
    {
        free( _obj->__Binary );
        __ABRTSTOP( "memory reserve miss" );
    }
    
    _obj->__Binary = BinaryNew;
    memset( &_obj->__Binary[_obj->__binarySize], 0x00, (_obj->__memOneBlockSize*SetBlockCount-_obj->__binarySize) );
    _obj->__memBlockCount = SetBlockCount;
}


static void SC_Binary_LockerInit( struct SC_Binary* obj )
{
    pthread_mutex_init( &obj->__locker, NULL );
}
static void SC_Binary_Lock( struct SC_Binary* obj )
{
    pthread_mutex_lock( &obj->__locker );
}
static void SC_Binary_UnLock( struct SC_Binary* obj )
{
    pthread_mutex_unlock( &obj->__locker );
}


//----------------CallBacks---------------------
void SC_Binary_LogInfo( SC_Object obj, struct SC_LogBuffer* LogBuffer )
{
    struct SC_Binary* _obj = (struct SC_Binary*)obj;
    struct SC_LogBuffer* _LogBuffer = (struct SC_LogBuffer*)LogBuffer;
    
    char msg[128] = {0};
    sprintf( msg, "Calledfunc(%s) :: binarySize(%zu) / ", __func__, _obj->__binarySize );
    
    SC_LogBuffer_AddData( _LogBuffer, msg );
}

SC_Object SC_Binary_CloneObj( SC_Object obj )
{
    struct SC_Binary* _obj = (struct SC_Binary*)obj;
    
    struct SC_Binary* CloneObj = (struct SC_Binary*)SC_Binary_AllocInit();
    CloneObj->__binarySize = _obj->__binarySize;
    CloneObj->__memBlockCount = _obj->__memBlockCount;
    CloneObj->__memOneBlockSize = _obj->__memOneBlockSize;
    
    void* tmpBin = realloc( CloneObj->__Binary, CloneObj->__memOneBlockSize*CloneObj->__memBlockCount );
    if( tmpBin == NULL )
    {
        free( CloneObj->__Binary );
        __ABRTSTOP( "memory reserve miss" );
    }
    CloneObj->__Binary = tmpBin;
    
    return CloneObj;
}

void SC_Binary_freeObj( SC_Object obj )
{
    struct SC_Binary* _obj = (struct SC_Binary*)obj;
    free( _obj->__Binary );
    
    pthread_mutex_destroy( &_obj->__locker );
}

void SC_Binary_ToJson( SC_Object obj, SC_Object JsonBuffer )
{
    struct SC_Binary* _obj = (struct SC_Binary*)obj;
        
    SC_String_InsertFormat( JsonBuffer, SC_IndexLast, "\"" );
    for ( size_t i = 0; i <= _obj->__binarySize; i++ ) 
    {
        SC_String_InsertFormat( JsonBuffer, SC_IndexLast, "%x", ((char*)_obj->__Binary)[i] );
    }
    SC_String_InsertFormat( JsonBuffer, SC_IndexLast, "\"" );
    
}

