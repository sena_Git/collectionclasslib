//
//  SC_Pool.c
//  SenaClass
//
//  Created by matsushita sena on 2018/12/22.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#include "SC_Pool.h"
#include "SC_RunTime.h"
#include "SC_Array.h"
#include "SC_String.h"

struct SC_Pool{
    struct SC_RunTime   __runtime;
    
    pthread_mutex_t     __locker;
    
    size_t              __currentLevel;
    SC_Object           __instanceArray;
};

//---------------------------------NoLockFunc---------------------------------------------
//void SC_Pool_Create_NL( void );
//void SC_Pool_Release_NL( void );

//--------------------------------CallBackFuncs-------------------------------------------
SC_Object SC_Pool_CloneObj( SC_Object obj );
void SC_Pool_freeObj( SC_Object obj );
void SC_Pool_ToJson( SC_Object obj, SC_Object JsonBuffer );
void SC_Pool_LogInfo( SC_Object obj, struct SC_LogBuffer* LogBuffer );
//-----LogInfo----
extern void SC_LogBuffer_AddData( struct SC_LogBuffer* LogBuffer, const char* AddLog );

//--------------------------------RunTime_Alloc-------------------------------------------
extern void* SC_RunTime_AllocInit( SC_TypeID typeId, size_t obj_size );

//--------------------------------Retain&Release------------------------------------------
extern void SC_RunTime_Retain( SC_Object obj );
extern void SC_RunTime_Release( SC_Object obj );

//---------------------------------OwnerPool----------------------------------------------
struct _Pool_Unit{    
    pthread_t               __threadId;
    
    size_t                  __poolCount;
    SC_Object               __poolArray;
};


struct _Pool_Owner{
    pthread_mutex_t         __locker;
    
    size_t                  __unitCount;
    size_t                  __blockCount;
    size_t                  __oneBlockSize;
    struct _Pool_Unit*      __poolUnit;
};

void SC_Pool_OwnerInit( void );
static SC_Index SC_Pool_IndexFind( pthread_t searchId );
static void SC_Pool_UnitSetBlock( size_t setBlockCount );

static struct _Pool_Owner OwPool = {0};



void SC_Pool_Alloc( void )
{    
    struct SC_Pool* _pool  = SC_RunTime_AllocInit( TypeId_Pool, sizeof( struct SC_Pool ) );
    _pool->__runtime.__CloneObj = SC_Pool_CloneObj;
    _pool->__runtime.__FreeObj = SC_Pool_freeObj;
    _pool->__runtime.__LogInfo = SC_Pool_LogInfo;
    _pool->__runtime.__ToJson = SC_Pool_ToJson;
    
    _pool->__instanceArray = SC_Array_AllocInit();
    
    pthread_t thisThreadId = pthread_self();
    
    SC_Index unitIndex = SC_Pool_IndexFind( thisThreadId );
    if( unitIndex == SC_IndexNone )
    {
        if( (OwPool.__oneBlockSize*OwPool.__blockCount)  <= OwPool.__unitCount+1 )
        {
            SC_Pool_UnitSetBlock( OwPool.__blockCount+1 );
        }
        
        OwPool.__poolUnit[OwPool.__unitCount].__threadId = thisThreadId;
        
        OwPool.__poolUnit[OwPool.__unitCount].__poolArray = SC_Array_AllocInit();
        SC_Array_Insert( OwPool.__poolUnit[OwPool.__unitCount].__poolArray, 0, _pool );
        OwPool.__poolUnit[OwPool.__unitCount].__poolCount++;
        
        OwPool.__unitCount++;
    }else{
        SC_Array_Insert( OwPool.__poolUnit[unitIndex].__poolArray, SC_IndexLast, _pool );
        _pool->__currentLevel = OwPool.__poolUnit[unitIndex].__poolCount;
        OwPool.__poolUnit[unitIndex].__poolCount++;
    }
}

 
void SC_Pool_Release( void )
{
    SC_Index unitIndex = SC_Pool_IndexFind( pthread_self() );
    if( unitIndex == SC_IndexNone  )
    {
        __DebugComent( "Unknown SC_Pool for Regist");
        return;
    }
    
    SC_Array_Remove( OwPool.__poolUnit[unitIndex].__poolArray, SC_IndexLast );
    OwPool.__poolUnit[unitIndex].__poolCount--;
    
    if( OwPool.__poolUnit[unitIndex].__poolCount == 0 )
    {
        SC_RunTime_Release( OwPool.__poolUnit[unitIndex].__poolArray );
        memmove( &OwPool.__poolUnit[unitIndex], &OwPool.__poolUnit[unitIndex+1], sizeof( struct _Pool_Unit )*(OwPool.__unitCount-unitIndex-1) );
        OwPool.__unitCount--;
        if( OwPool.__unitCount < (OwPool.__oneBlockSize*(OwPool.__blockCount-1)) )
        {
            SC_Pool_UnitSetBlock( OwPool.__blockCount-1 );
        }
    }
}


void SC_Pool_CurrentUp( SC_Object obj )
{
    SC_Index unitIndex = SC_Pool_IndexFind( pthread_self() );
    if( unitIndex == SC_IndexNone  )
    {
        __DebugComent( "Unknown SC_Pool for Regist");
        return;
    }
    
    if( ((SC_Index)(OwPool.__poolUnit[unitIndex].__poolCount-1))-1 < 0 )
    {
        __DebugComent( "Unknown SC_Pool for Regist");
        return;
    }
    
    struct SC_Pool* poolArray = SC_Array_GetObject( OwPool.__poolUnit[unitIndex].__poolArray, ((SC_Index)(OwPool.__poolUnit[unitIndex].__poolCount-1))-1 );
    SC_Array_Insert( poolArray->__instanceArray, SC_IndexLast, obj );
}


//-----------------------------------private-----------------------------------------------
void SC_Pool_OwnerInit( void )
{
    pthread_mutex_init( &OwPool.__locker, NULL );
    OwPool.__blockCount = 0;
    OwPool.__oneBlockSize = SC_MALLOCSIZE/sizeof( struct _Pool_Unit );
}

static SC_Index SC_Pool_IndexFind( pthread_t searchId )
{
    SC_Index unitIndex = SC_IndexNone;
    for ( SC_Index i=0; i<OwPool.__unitCount; i++) 
    {
        if( pthread_equal( searchId, OwPool.__poolUnit[i].__threadId ) != 0 )
        {
            unitIndex = i;
            break;
        }
    }
    return unitIndex;
}

SC_Object SC_Pool_Autorelease( SC_Object obj )
{    
    SC_Index unitIndex = SC_Pool_IndexFind( pthread_self() );
    if( unitIndex == SC_IndexNone )
    {
        __DebugComent( "Unknown SC_Pool for Regist" );
        return obj;
    }
    
    struct SC_Pool* _pool = SC_Array_GetObject( OwPool.__poolUnit[unitIndex].__poolArray, SC_IndexLast );
    SC_Array_Insert( _pool->__instanceArray, SC_IndexLast, obj );
    
    return obj;
}

static void SC_Pool_UnitSetBlock( size_t setBlockCount )
{
    struct _Pool_Unit* poolUnitNew = (struct _Pool_Unit*)realloc( OwPool.__poolUnit, sizeof( struct _Pool_Unit* )*(OwPool.__oneBlockSize*setBlockCount) );
    if( poolUnitNew == NULL )
    {
        __ABRTSTOP( "memory reserve miss" );
    }
    
    OwPool.__poolUnit = poolUnitNew;
    memset( &OwPool.__poolUnit[OwPool.__unitCount], 0x00, sizeof( struct _Pool_Unit* )*((OwPool.__oneBlockSize*setBlockCount)-OwPool.__unitCount) );
    OwPool.__blockCount = setBlockCount;
}


//----------------CallBacks---------------------
SC_Object SC_Pool_CloneObj( SC_Object obj )
{
    __DebugComent( "You can't clone pool Object." );
    return NULL;
}

void SC_Pool_freeObj( SC_Object obj )
{
    struct SC_Pool* _obj = (struct SC_Pool*)obj;
    
    SC_RunTime_Release( _obj->__instanceArray );
    pthread_mutex_destroy( &_obj->__locker );
}

void SC_Pool_ToJson( SC_Object obj, SC_Object JsonBuffer )
{
    SC_Index unitIndex = SC_Pool_IndexFind( pthread_self() );
    if( unitIndex == SC_IndexNone )
    {
        __DebugComent( "Unknown release pool" );
    }
    
    struct SC_Pool* _obj = (struct SC_Pool*)obj;
    
    SC_String_Insert( JsonBuffer, SC_IndexLast, "{" );
    
    for ( SC_Index i = 0; i < SC_Array_GetCount( _obj->__instanceArray ); i++ ) 
    {
        struct SC_RunTime* tmpObj = (struct SC_RunTime*)SC_Array_GetObject( _obj->__instanceArray, i );
        
        if( 0<i )
        {
            SC_String_Insert( JsonBuffer, SC_IndexLast, "," );
        }
        
        tmpObj->__ToJson( tmpObj, JsonBuffer );
    }
    
    SC_String_Insert( JsonBuffer, SC_IndexLast, "}");
}

void SC_Pool_LogInfo( SC_Object obj, struct SC_LogBuffer* LogBuffer )
{
    struct SC_Pool* _obj = (struct SC_Pool*)obj;
    struct SC_LogBuffer* _LogBuffer = (struct SC_LogBuffer*)LogBuffer;
    
    char msg[128] = {0};
    sprintf( msg, "Calledfunc(%s) :: count(%zu) / ", __func__, SC_Array_GetCount( _obj->__instanceArray ) );
    
    SC_LogBuffer_AddData( _LogBuffer, msg );
}