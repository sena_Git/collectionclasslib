//
//  SC_String.c
//  CollectionClass
//
//  Created by matsushita sena on 2018/08/19.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#include "SC_String.h"
#include "SC_Integer.h"
#include "SC_Float.h"
#include "SC_RunTime.h"

struct SC_String {
    struct SC_RunTime    __runtime;
    
    pthread_mutex_t      __locker;
    
    size_t               __len;
    size_t               __memBlockCount;
    size_t               __memOneBlockSize;
    char*                __str;
    char*                __tmpCstr;
};



#ifdef SC_TYPECHECK
    static void SC_String_TypeChecker( SC_TypeID* objId, const char* FuncName );
    #define __String_TypeChecker( obj ) (SC_String_TypeChecker( (SC_TypeID*)obj, __func__ ))
#else 
    #define __String_TypeChecker( ... )
#endif

static void SC_String_SetBlock( SC_Object obj, size_t SetBlockCount );

size_t SC_Format_toStr( char* outPointer, char* buffer, const char* format, va_list ap );

//---------------------------------NoLockFunc---------------------------------------------
SC_Object SC_String_AllocInit_NL( const char* string );
void SC_String_Insert_NL( SC_Object obj, SC_Index index, const char* string );
void SC_String_InsertObj_NL( SC_Object obj, SC_Index index, SC_Object InputObj );
void SC_String_Remove_NL( SC_Object obj, SC_Index fromIndex, size_t size );
void SC_String_Replace_NL( SC_Object obj, const char* target, const char* repl );
SC_Boolean SC_String_Compare_NL( SC_Object obj, const char* sarch );
SC_Index SC_String_IndexFind_NL( SC_Object obj, const char* sarch );
char SC_String_GetAtIndex_NL( SC_Object obj, SC_Index index );
void SC_String_OutPut_NL(  const char* AftDisp, SC_Object obj, const char* BfoDisp );
size_t SC_String_GetLength_NL( SC_Object obj );
void SC_String_CopyStr_NL( SC_Object obj, char* str, size_t getLen );
const char* SC_String_GetCstr_NL( SC_Object obj );

//-------------------------------MutexLocker---------------------------------------------
static void SC_String_LockerInit( struct SC_String* obj );
static void SC_String_Lock( struct SC_String* obj );
static void SC_String_UnLock( struct SC_String* obj );

//--------------------------------CallBackFuncs-------------------------------------------
SC_Object SC_String_CloneObj( SC_Object obj );
void SC_String_freeObj( SC_Object obj );
void SC_String_ToJson( SC_Object obj, SC_Object JsonBuffer );
void SC_String_LogInfo( SC_Object obj, struct SC_LogBuffer* LogBuffer );
//-----LogInfo----
extern void SC_LogBuffer_AddData( struct SC_LogBuffer* LogBuffer, const char* AddLog );

//--------------------------------RunTime_Alloc-------------------------------------------
extern void* SC_RunTime_AllocInit( SC_TypeID typeId, size_t obj_size );

//-------------------------------PoolAutoRelease------------------------------------------
extern SC_Object SC_Pool_Autorelease( SC_Object SetObj );




SC_Object SC_String_AllocInit( const char* string )
{
    struct SC_String* _obj = SC_String_AllocInit_NL( string );
    SC_String_LockerInit( _obj );
    
    return _obj;
}

SC_Object SC_String_AllocInitAutorelease( const char* string )
{
    return SC_Pool_Autorelease( SC_String_AllocInit( string ) );
}

SC_Object SC_String_AllocInitFormat( const char* format, ... )
{
    va_list ap;
    va_start( ap, format );
    char buf[64] = {0};
    char* string = buf;
    size_t len = SC_Format_toStr( string, buf, format, ap );
    va_end( ap );
    
    SC_Object* _obj = SC_String_AllocInit( string );
    
    if( 64 <= len )
    {
        free( string );
    }
    return _obj;
}

SC_Object SC_String_AllocInitFormatAutorelease( const char* format, ... )
{
    va_list ap;
    va_start( ap, format );
    char buf[64] = {0};
    char* string = buf;
    size_t len = SC_Format_toStr( string, buf, format, ap );
    va_end( ap );
    
    SC_Object* _obj = SC_Pool_Autorelease( SC_String_AllocInit( string ) );
    
    if( 64 <= len )
    {
        free( string );
    }
    return _obj;
}

SC_Object SC_String_AllocInit_NL( const char* string )
{    
    struct SC_String* _obj = SC_RunTime_AllocInit( TypeId_String, sizeof( struct SC_String ) );
    _obj->__runtime.__CloneObj = SC_String_CloneObj;
    _obj->__runtime.__FreeObj = SC_String_freeObj;
    _obj->__runtime.__ToJson = SC_String_ToJson;
    _obj->__runtime.__LogInfo = SC_String_LogInfo;
    _obj->__memOneBlockSize  = SC_MALLOCSIZE/sizeof( char );
    
    size_t len = strlen( string );
    
    SC_String_SetBlock( _obj, (len/_obj->__memOneBlockSize)+1 );
    
    memcpy( _obj->__str, string, sizeof( char )*len );
    memset( &(_obj->__str[len]), 0x00, sizeof( char )* (_obj->__memOneBlockSize*_obj->__memBlockCount-len) );

    _obj->__len = len;
    return _obj;
}



void SC_String_Insert( SC_Object obj, SC_Index index, const char* string )
{
    __String_TypeChecker( obj );
    
    SC_String_Lock( obj );
    SC_String_Insert_NL( obj, index, string );
    SC_String_UnLock( obj );
    
}

void SC_String_InsertFormat( SC_Object obj, SC_Index index, const char* format, ... )
{
    __String_TypeChecker( obj );
    va_list ap;
    va_start( ap, format );
    char buf[64] = {0};
    char* string = buf;
    size_t len = SC_Format_toStr( string, buf, format, ap );
    va_end( ap );
    
    SC_String_Insert( obj, index, string );
    
    if( 64 <= len )
    {
        free( string );
    }
}

void SC_String_Insert_NL( SC_Object obj, SC_Index index, const char* string )
{
    struct SC_String* _obj = (struct SC_String*)obj;
    
    if( index == SC_IndexLast )
    {
        index = (SC_Index)_obj->__len;
    }else if( (index < 0) || (_obj->__len < index) )
    {
        __DebugComent( "index is wrong" );
        return;
    }
    
    size_t AddLen = strlen( string );
    
    if( (_obj->__memOneBlockSize*_obj->__memBlockCount) < (_obj->__len+AddLen)+1 )
    {
        SC_String_SetBlock( _obj, ((_obj->__len + AddLen)/_obj->__memOneBlockSize)+1 );
    }
    
    memmove( &(_obj->__str[index+AddLen]), &(_obj->__str[index]), sizeof( char )*(_obj->__len-index) );
    memcpy( &(_obj->__str[index]), string, sizeof( char )*(AddLen) );
    
    _obj->__len += AddLen;
}



void SC_String_InsertObj( SC_Object obj, SC_Index index, SC_Object InputObj )
{
    __String_TypeChecker( obj );
    
    SC_String_Lock( obj );
    SC_String_InsertObj_NL( obj, index, InputObj );
    SC_String_UnLock( obj );
}

void SC_String_InsertObj_NL( SC_Object obj, SC_Index index, SC_Object InputObj )
{     
    char TmpBuffer[64];
    if( InputObj == NULL )
    {
        return;
    }
    struct SC_RunTime* _InputObj = (struct SC_RunTime*)InputObj;
    
    if( _InputObj->__typeId == TypeId_String )
    {
        struct SC_String* _InputObj = (struct SC_String*)InputObj;
        SC_String_Insert_NL( obj, index, _InputObj->__str );
        return;
    }
    switch( _InputObj->__typeId ){
        case TypeId_Integer:
            sprintf( TmpBuffer, "%lld", SC_Integer_GetValue( InputObj ) );
            SC_String_Insert_NL( obj, index, TmpBuffer );
            break;
        case TypeId_Float:
            sprintf( TmpBuffer, "%lf", SC_Float_GetValue( InputObj ) );
            SC_String_Insert_NL( obj, index, TmpBuffer );
            break;
        default:
            __DebugTypeEr( __func__ );
    }
}



void SC_String_Remove( SC_Object obj, SC_Index fromIndex, size_t size )
{
    __String_TypeChecker( obj );
    
    SC_String_Lock( obj );
    SC_String_Remove_NL( obj, fromIndex, size );
    SC_String_UnLock( obj );
}

void SC_String_Remove_NL( SC_Object obj, SC_Index fromIndex, size_t size )
{
    struct SC_String* _obj = (struct SC_String*)obj;
    
    if( fromIndex < 0 )
    {
        __DebugComent( "fromIndex is wrong" );
        return;
    }
    if( _obj->__len < fromIndex+size )
    {
        size = _obj->__len-fromIndex;
    }
    
    memmove( &(_obj->__str[fromIndex]), &(_obj->__str[fromIndex+size]), sizeof( char )*(_obj->__len-(fromIndex+size)) );
    memset( &(_obj->__str[_obj->__len-size]), 0x00, sizeof( char )*size );
    
    _obj->__len -= size;
    
    if( _obj->__len < ( _obj->__memOneBlockSize*(_obj->__memBlockCount-1)) )
    {
        SC_String_SetBlock( _obj, _obj->__memBlockCount-1 );
    }
}



void SC_String_Replace( SC_Object obj, const char* target, const char* repl )
{
    __String_TypeChecker( obj );
    
    SC_String_Lock( obj );
    SC_String_Replace_NL( obj, target, repl );
    SC_String_UnLock( obj );
}

void SC_String_Replace_NL( SC_Object obj, const char* target, const char* repl )
{
    struct SC_String* _obj = (struct SC_String*)obj;
    
    size_t targetLen = strlen( target );
    
    SC_Boolean hit = SC_FALSE;
    for ( SC_Index i = 0; i <= (_obj->__len-targetLen); i++ ) 
    {
        if( _obj->__str[i] == target[0] )
        {
            hit = SC_TRUE;
            for ( size_t ii = 1; ii < targetLen; ii++) 
            {
                if( _obj->__str[i+ii] != target[0+ii] )
                {
                    hit = SC_FALSE;
                    break;
                }
            }
            if ( hit == SC_TRUE ) 
            {
                SC_String_Remove_NL( _obj, i, targetLen );
                SC_String_Insert_NL( _obj, i, repl );
            }
        }
    }
}



SC_Boolean SC_String_Compare( SC_Object obj, const char* sarch )
{
    __String_TypeChecker( obj );
    SC_String_Lock( obj );
    SC_Boolean hit = SC_String_Compare_NL( obj, sarch );
    SC_String_UnLock( obj );
    
    return hit;
}

SC_Boolean SC_String_CompareFormat( SC_Object obj, const char* format, ... )
{
    __String_TypeChecker( obj );
    va_list ap;
    va_start( ap, format );
    char buf[64] = {0};
    char* string = buf;
    size_t len = SC_Format_toStr( string, buf, format, ap );
    va_end( ap );
    
    SC_Boolean hit = SC_String_Compare( obj, string );
    
    if( 64 <= len )
    {
        free( string );
    }
    return hit;
}

SC_Boolean SC_String_Compare_NL( SC_Object obj, const char* sarch )
{
    struct SC_String* _obj = (struct SC_String*)obj;
    
    size_t targetLen = strlen( sarch ); 
    
    if( targetLen != _obj->__len )
    {
        return SC_FALSE;
    }
    
    SC_Boolean hit = SC_FALSE;
    for ( size_t i = 0; i < _obj->__len; i++ ) 
    {
        if( _obj->__str[i] == sarch[i] )
        {
            hit = SC_TRUE;
        }else{
            hit = SC_FALSE;
            break;
        }
    }
    
    return hit;
}



SC_Index SC_String_IndexFind( SC_Object obj, const char* sarch )
{
    __String_TypeChecker( obj );
    SC_String_Lock( obj );
    SC_Index Index = SC_String_IndexFind_NL( obj, sarch );
    SC_String_UnLock( obj );
    
    return Index;
}

SC_Index SC_String_IndexFindFormat( SC_Object obj, const char* format, ... )
{
    __String_TypeChecker( obj );
    va_list ap;
    va_start( ap, format );
    char buf[64] = {0};
    char* string = buf;
    size_t len = SC_Format_toStr( string, buf, format, ap );
    va_end( ap );
    
    SC_Index Index = SC_String_IndexFind( obj, string );
    
    if( 64 <= len )
    {
        free( string );
    }
    return Index;
}

SC_Index SC_String_IndexFind_NL( SC_Object obj, const char* sarch )
{
    struct SC_String* _obj = (struct SC_String*)obj;
    
    size_t sarchLen = strlen( sarch );
    
    SC_Index index = SC_IndexNone;
    for ( SC_Index i = 0; i <= (_obj->__len - sarchLen); i++ ) 
    {
        if( _obj->__str[i] == sarch[0] )
        {
            if( memcmp( &_obj->__str[i], sarch, sizeof( char )*sarchLen ) == 0 )
            {
                index = i;
                break;
            }
        }
    }
    return index;
}



char SC_String_GetAtIndex( SC_Object obj, SC_Index index )
{
    __String_TypeChecker( obj );
    
    SC_String_Lock( obj );
    char Character = SC_String_GetAtIndex_NL( obj, index );
    SC_String_UnLock( obj );
    
    return Character;
}

char SC_String_GetAtIndex_NL( SC_Object obj, SC_Index index )
{
    struct SC_String* _obj = (struct SC_String*)obj;
        
    if( index == SC_IndexLast )
    {
        index = (SC_Index)_obj->__len;
    }
    
    if( (index < 0) || (_obj->__len < index) )
    {
        __DebugComent( "index is wrong" );
        return 0;
    }
    
    return _obj->__str[index];
}



void SC_String_OutPut( const char* BfoDisp, SC_Object obj, const char* AftDisp )
{
    __String_TypeChecker( obj );
    
    SC_String_Lock( obj );
    SC_String_OutPut_NL( BfoDisp, obj, AftDisp );
    SC_String_UnLock( obj );
}

void SC_String_OutPut_NL( const char* BfoDisp, SC_Object obj, const char* AftDisp )
{
    struct SC_String* _obj = (struct SC_String*)obj;
    printf( "%s%s%s", BfoDisp, _obj->__str, AftDisp );
}



void SC_String_CopyStr( SC_Object obj, char* str, size_t getLen ) //size_t len
{
    __String_TypeChecker( obj );
    
    SC_String_Lock( obj );
    SC_String_CopyStr_NL( obj, str, getLen );
    SC_String_UnLock( obj );
}

void SC_String_CopyStr_NL( SC_Object obj, char* str, size_t getLen )
{
    struct SC_String* _obj = (struct SC_String*)obj;
    
    if( _obj->__len < getLen )
    {
        getLen = _obj->__len+1;
    }
    memcpy( str, _obj->__str, sizeof( char )*getLen-1 );
    str[getLen-1] = 0x00;
}



const char* SC_String_GetCstr( SC_Object obj )
{
    __String_TypeChecker( obj );
    
    SC_String_Lock( obj );
    const char* Cstr = SC_String_GetCstr_NL( obj );
    SC_String_UnLock( obj );
    
    return Cstr;
}

const char* SC_String_GetCstr_NL( SC_Object obj )
{
    struct SC_String* _obj = (struct SC_String*)obj;
    
    char* tmpStr = (char*)realloc( _obj->__tmpCstr, sizeof( char )*_obj->__len+1 );
    if( tmpStr == NULL )
    {
        free( _obj->__str );
        __ABRTSTOP( "memory reserve miss" );
    }
    _obj->__tmpCstr = tmpStr;
    
    memcpy( _obj->__tmpCstr, _obj->__str, sizeof( char )*_obj->__len );
    _obj->__tmpCstr[_obj->__len] = 0x00;
    
    return _obj->__tmpCstr;
}



size_t SC_String_GetLength( SC_Object obj )
{
    __String_TypeChecker( obj );

    SC_String_Lock( obj );
    size_t len = SC_String_GetLength_NL( obj );
    SC_String_UnLock( obj );
    
    return len;
}

size_t SC_String_GetLength_NL( SC_Object obj )
{
    struct SC_String* _obj = (struct SC_String*)obj;

    return _obj->__len;
}



//-----------------------------------private-----------------------------------------------
#ifdef SC_TYPECHECK
static void SC_String_TypeChecker( SC_TypeID* objId, const char* FuncName )
{
    if( objId == NULL )
    {
        __DebugTypeEr( FuncName );
    }
    
    if( *objId != TypeId_String )
    {
        __DebugTypeEr( FuncName );
    }

}
#endif


static void SC_String_SetBlock( SC_Object obj, size_t SetBlockCount )
{
    struct SC_String* _obj = (struct SC_String*)obj;
    
    void* StrNew = realloc( _obj->__str, sizeof( char )*(_obj->__memOneBlockSize*SetBlockCount) );
    if( StrNew == NULL )
    {
        free( _obj->__str );
        __ABRTSTOP( "memory reserve miss" );
    }
    
    _obj->__str = StrNew;
    memset( &_obj->__str[_obj->__len], 0x00, sizeof( char )*(_obj->__memOneBlockSize*SetBlockCount-_obj->__len) );
    _obj->__memBlockCount = SetBlockCount;
}


size_t SC_Format_toStr( char* outPointer, char* buffer, const char* format, va_list ap )
{ 
    va_list apCopy;
    va_copy( apCopy, ap );
    
    SC_Index len;
    
    len = vsnprintf( buffer, 64, format, ap );
    if( len < 0 )
    {
        __DebugComent( "vsnprintf Er" );
        va_end( apCopy );
        outPointer = buffer;
        return 0;
    }
    
    if( len <= 64 )
    {
        outPointer = buffer;
    }else{
        outPointer = (char*)malloc( sizeof( char )*(len+1) );
        len = vsnprintf( outPointer, len+1, format, apCopy ); 
        if( len < 0 )
        {
            __DebugComent( "vsnprintf Er" );
            free( outPointer );
            outPointer = buffer;
            len = 0;
        }
    }
    va_end( apCopy );
    
    return  len;
}


static void SC_String_LockerInit( struct SC_String* obj )
{
    pthread_mutex_init( &obj->__locker, NULL );
}

static void SC_String_Lock( struct SC_String* obj )
{
    pthread_mutex_lock( &obj->__locker );
}

static void SC_String_UnLock( struct SC_String* obj )
{
    pthread_mutex_unlock( &obj->__locker );
}


//----------------CallBacks---------------------
SC_Object SC_String_CloneObj( SC_Object obj )
{
    struct SC_String* _obj = (struct SC_String*)obj;    
    return SC_String_AllocInit_NL( _obj->__str );
}


void SC_String_freeObj( SC_Object obj )
{
    struct SC_String* _obj = (struct SC_String*)obj;
    free( _obj->__str );
    
    pthread_mutex_destroy( &_obj->__locker );
}


void SC_String_LogInfo( SC_Object obj, struct SC_LogBuffer* LogBuffer )
{
    struct SC_String* _obj = (struct SC_String*)obj;
    
    char msg[128] = {0};
    sprintf( msg, "Calledfunc(%s) :: str_ren(%zu) / ", __func__,_obj->__len );
    
    SC_LogBuffer_AddData( LogBuffer, msg );
}


void SC_String_ToJson( SC_Object obj, SC_Object JsonBuffer )
{
    struct SC_String* _obj = (struct SC_String*)obj;
    
    SC_String_Insert( JsonBuffer, SC_IndexLast, "\"" );
    SC_String_Insert( JsonBuffer, SC_IndexLast, _obj->__str );
    SC_String_Insert( JsonBuffer, SC_IndexLast, "\"" );
}