//
//  SC_Integer.h
//  CollectionClass
//
//  Created by matsushita sena on 2018/08/19.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#ifndef SC_Integer_h
#define SC_Integer_h

#include "SC_Type.h"


SC_Object SC_Integer_AllocInit( SC_SInt64 value );
SC_Object SC_Integer_AllocInitAutorelease( SC_SInt64 value );

SC_SInt64 SC_Integer_GetValue( SC_Object obj );

void SC_Integer_ChangeValue( SC_Object obj, SC_SInt64 Tovalue );

#endif /* SC_Integer_h */