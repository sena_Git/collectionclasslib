//
//  Array.c
//  CollectionClass
//
//  Created by matsushita sena on 2018/08/14.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#include "SC_Array.h"
#include "SC_RunTime.h"
#include "SC_Common.h"
#include "SC_Integer.h"
#include "SC_Float.h"
#include "SC_String.h"

struct SC_Array {
    struct SC_RunTime   __runtime;
    
    pthread_mutex_t     __locker;
    
    size_t              __count;            //
    size_t              __memOneBlockSize;  //32
    size_t              __memBlockCount;    //__memOneBlockSizeの数
    SC_Object*          __array;            //__memOneBlockSize*__memBlockCountのサイズでリサイズ
};



#ifdef SC_TYPECHECK
    static void SC_Array_TypeChecker( SC_TypeID* objId, const char* FuncName );
    #define __Array_TypeChecker( obj ) (SC_Array_TypeChecker( (SC_TypeID*)obj, __func__ ))
#else 
    #define __Array_TypeChecker( ... )
#endif


static void SC_Array_SetBlock( SC_Object obj, size_t AddBlockCount );

//---------------------------------NoLockFunc---------------------------------------------
SC_Object SC_Array_AllocInit_NL( void );
void SC_Array_Insert_NL( SC_Object obj, SC_Index index, SC_Object InputObj );
void SC_Array_Replace_NL( SC_Object obj, SC_Index index, SC_Object repl );
void SC_Array_Remove_NL( SC_Object obj, SC_Index index );
SC_Index SC_Array_IndexFind_NL( SC_Object obj, SC_Object sarch );
size_t SC_Array_GetCount_NL( SC_Object obj );
SC_Object SC_Array_GetObject_NL( SC_Object obj, SC_Index index );
SC_Object SC_Array_CopyObj_NL( SC_Object obj, SC_Index index );


//-------------------------------MutexLocker---------------------------------------------
static inline void SC_Array_LockerInit( struct SC_Array* obj );
static inline void SC_Array_Lock( struct SC_Array* obj );
static inline void SC_Array_UnLock( struct SC_Array* obj );

//--------------------------------CallBackFuncs-------------------------------------------
SC_Object SC_Array_CloneObj( SC_Object obj );
void SC_Array_freeObj( SC_Object obj );
void SC_Array_ToJson( SC_Object obj, SC_Object JsonBuffer );
void SC_Array_LogInfo( SC_Object obj, struct SC_LogBuffer* LogBuffer );
//-----LogInfo----
extern void SC_LogBuffer_AddData( struct SC_LogBuffer* LogBuffer, const char* AddLog );

//--------------------------------RunTime_Alloc-------------------------------------------
extern void* SC_RunTime_AllocInit( SC_TypeID typeId, size_t obj_size );

//--------------------------------Retain&Release------------------------------------------
extern void SC_RunTime_Retain( SC_Object obj );
extern void SC_RunTime_Release( SC_Object obj );

//-------------------------------PoolAutoRelease------------------------------------------
extern SC_Object SC_Pool_Autorelease( SC_Object SetObj );


SC_Object SC_Array_AllocInit( void )
{
    struct SC_Array* obj = SC_Array_AllocInit_NL();
    SC_Array_LockerInit( obj );
    
    return obj;
}

SC_Object SC_Array_AllocInitAutorelease( void )
{
    return  SC_Pool_Autorelease( SC_Array_AllocInit() );
}

SC_Object SC_Array_AllocInit_NL( void )
{
    struct SC_Array* _obj = SC_RunTime_AllocInit( TypeId_Array, sizeof( struct SC_Array ) );
    _obj->__runtime.__FreeObj = SC_Array_freeObj;
    _obj->__runtime.__CloneObj = SC_Array_CloneObj;
    _obj->__runtime.__ToJson = SC_Array_ToJson;
    _obj->__runtime.__LogInfo = SC_Array_LogInfo;
    _obj->__count  = 0;
    _obj->__memOneBlockSize  = SC_MALLOCSIZE/sizeof( SC_Object );
    _obj->__memBlockCount  = 1;
    SC_Array_SetBlock( _obj, _obj->__memOneBlockSize*_obj->__memBlockCount );
        
    return _obj;
}



void SC_Array_Insert( SC_Object obj, SC_Index index, SC_Object InputObj )
{
    __Array_TypeChecker( obj );
    
    SC_Array_Lock( obj );
    SC_Array_Insert_NL( obj, index, InputObj );
    SC_Array_UnLock( obj );
}

void SC_Array_Insert_NL( SC_Object obj, SC_Index index, SC_Object InputObj )
{  
    struct SC_Array* _obj = (struct SC_Array*)obj;
    
    if( index == SC_IndexLast )
    {
        index = (SC_Index)_obj->__count;
    }else if( (index < 0) || (_obj->__count < index) ){
        __DebugComent( "index is wrong" );
        return;
    }
    
    if( ( _obj->__memOneBlockSize*_obj->__memBlockCount)  <= index )
    {
        SC_Array_SetBlock( _obj, _obj->__memBlockCount+1 );
    }
    
    memmove( &(_obj->__array[index+1]), &(_obj->__array[index]), sizeof( SC_Object )*(_obj->__count-index) );
    SC_RunTime_Retain( InputObj );
    _obj->__array[index] = InputObj;

    _obj->__count++;
}



void SC_Array_Replace( SC_Object obj, SC_Index index, SC_Object repl )
{
    __Array_TypeChecker( obj );
    
    SC_Array_Lock( obj );
    SC_Array_Replace_NL( obj, index, repl );
    SC_Array_UnLock( obj );
}

void SC_Array_Replace_NL( SC_Object obj, SC_Index index, SC_Object repl )
{
    struct SC_Array* _obj = (struct SC_Array*)obj;
    
    if( index == SC_IndexLast )
    {
        index = (SC_Index)_obj->__count-1;
    }
    else if( (index<0) || ((_obj->__count-1)<index) )
    {
        __DebugComent( "index is wrong" );
        return;
    }
    
    if( _obj->__array[index] != NULL )
    {
        SC_RunTime_Release( _obj->__array[index] );
    }
    
    SC_RunTime_Retain( repl );
    _obj->__array[index] = repl;
}



void SC_Array_Remove( SC_Object obj, SC_Index index )
{
    __Array_TypeChecker( obj );
    
    SC_Array_Lock( obj );
    SC_Array_Remove_NL( obj, index );
    SC_Array_UnLock( obj );
}


void SC_Array_Remove_NL( SC_Object obj, SC_Index index )
{
    struct SC_Array* _obj = (struct SC_Array*)obj;
    
    if( index == SC_IndexLast )
    {
        index = (SC_Index)_obj->__count-1;
    }
    else if( (index < 0) || (_obj->__count-1 < index) )
    {
        __DebugComent( "index is wrong" );
        return;
    }
    
    SC_RunTime_Release( _obj->__array[index] );
    if( (_obj->__count-1)-index != 0 )
    {
        memmove( &(_obj->__array[index]), &(_obj->__array[index+1]), sizeof( SC_Object )*((_obj->__count-1)-index) );
        _obj->__array[_obj->__count-1] = NULL;
    }else{
        _obj->__array[index] = NULL;
    }
    
    _obj->__count--;
    
    if( _obj->__count < ( _obj->__memOneBlockSize*(_obj->__memBlockCount-1)) )
    {
        SC_Array_SetBlock( _obj, _obj->__memBlockCount-1 );
    }
}



SC_Index SC_Array_IndexFind( SC_Object obj, SC_Object sarch )
{
    __Array_TypeChecker( obj );
    
    SC_Array_Lock( obj );
    SC_Index index = SC_Array_IndexFind_NL( obj, sarch );
    SC_Array_UnLock( obj );
    
    return index;
}

SC_Index SC_Array_IndexFind_NL( SC_Object obj, SC_Object sarch )
{
    struct SC_Array* _obj = (struct SC_Array*)obj;
    
    SC_Index resault = SC_IndexNone;
    for ( SC_Index i = 0; i <= SC_Array_GetCount_NL( obj ); i++ ) 
    {
        if( _obj->__array[i] == sarch )
        {
            resault = i;
            break;
        }
    }
    return SC_IndexLast;
}



size_t SC_Array_GetCount( SC_Object obj )
{
    __Array_TypeChecker( obj );
    
    SC_Array_Lock( obj );
    size_t count = SC_Array_GetCount_NL( obj );
    SC_Array_UnLock( obj );
    
    return count;
}

size_t SC_Array_GetCount_NL( SC_Object obj )
{
    struct SC_Array* _obj = (struct SC_Array*)obj;
    
    return _obj->__count;
}



SC_Object SC_Array_GetObject( SC_Object obj, SC_Index index )
{
    __Array_TypeChecker( obj );
    
    SC_Array_Lock( obj );
    SC_Object OutputObj = SC_Array_GetObject_NL( obj, index );
    SC_Array_UnLock( obj );
    
    return OutputObj;
}

SC_Object SC_Array_GetObject_NL( SC_Object obj, SC_Index index )
{
    struct SC_Array* _obj = (struct SC_Array*)obj;
    
    if( index == SC_IndexLast )
    {
        index = (SC_Index)_obj->__count-1;
    }
    
    if( _obj->__count <= index )
    {
        __DebugComent( "index over" );
        return NULL;
    }
        
    return _obj->__array[index];
}



SC_Object SC_Array_CopyObj( SC_Object obj, SC_Index index )
{
    __Array_TypeChecker( obj );
    
    SC_Array_Lock( obj );
    SC_Object CopyObj = SC_Array_CopyObj_NL( obj, index );
    SC_Array_UnLock( obj );
    return CopyObj;
}

SC_Object SC_Array_CopyObjAutorelease( SC_Object obj, SC_Index index )
{
    return SC_Pool_Autorelease( SC_Array_CopyObj( obj, index ) );
}

SC_Object SC_Array_CopyObj_NL( SC_Object obj, SC_Index index )
{
    struct SC_Array* _obj = (struct SC_Array*)obj;
    
    if( _obj->__count <= index )
    {
        __DebugComent( "index over" );
        return NULL;
    }
    
    SC_Object CopyObj = SC_CloneObj( _obj->__array[index] );
    SC_RunTime_Release( CopyObj );
    return CopyObj;
}



//-----------------------------------private-----------------------------------------------
static void SC_Array_SetBlock(SC_Object obj, size_t SetBlockCount)
{
    struct SC_Array* _obj = (struct SC_Array*)obj;
    
    if( SetBlockCount == 0 )
    {
        SetBlockCount = 1;
    }
    
    SC_Object ArrayNew = (SC_Object*)realloc( _obj->__array, sizeof( SC_Object )*_obj->__memOneBlockSize*SetBlockCount );
    if( ArrayNew == NULL )
    {
        free( _obj->__array );
        __ABRTSTOP( "memory reserve miss" );
    }
    
    _obj->__array = ArrayNew;
    memset( &_obj->__array[_obj->__count], 0x00, sizeof( SC_Object )*(_obj->__memOneBlockSize*SetBlockCount-_obj->__count) );
    _obj->__memBlockCount = SetBlockCount;
}


#ifdef SC_TYPECHECK
static void SC_Array_TypeChecker( SC_TypeID* objId, const char* FuncName )
{
    if( objId == NULL )
    {
        __DebugTypeEr( FuncName );
    }
    if( *objId != TypeId_Array )
    {
        __DebugTypeEr( FuncName );
    }
}
#endif


static inline void SC_Array_LockerInit( struct SC_Array* obj )
{
    pthread_mutex_init( &obj->__locker, NULL );
}

static inline void SC_Array_Lock( struct SC_Array* obj )
{
    pthread_mutex_lock( &obj->__locker );
}

static inline void SC_Array_UnLock( struct SC_Array* obj )
{
    pthread_mutex_unlock( &obj->__locker );
}



//----------------CallBacks---------------------
void SC_Array_LogInfo( SC_Object obj, struct SC_LogBuffer* LogBuffer )
{
    struct SC_Array* _obj = (struct SC_Array*)obj;
    
    char msg[128] = {0};
    sprintf( msg, "Calledfunc(%s) :: count(%zu) / memBlockCount(%zu) / ", __func__, _obj->__count, _obj->__memBlockCount );
    
    SC_LogBuffer_AddData( LogBuffer, msg );
}


void SC_Array_ToJson( SC_Object obj, SC_Object JsonBuffer )
{
    struct SC_Array* _obj = (struct SC_Array*)obj;
        
    SC_String_Insert( JsonBuffer, SC_IndexLast, "[" );
    
    for (int i = 0; i < _obj->__count; i++) 
    {
        struct SC_RunTime* tmpObj = (struct SC_RunTime*)_obj->__array[i];
        
        if( 0<i )
        {
            SC_String_Insert( JsonBuffer, SC_IndexLast, "," );
        }
        
        tmpObj->__ToJson( tmpObj, JsonBuffer );
    
    }
    
    SC_String_Insert( JsonBuffer, SC_IndexLast, "]" );
}


SC_Object SC_Array_CloneObj( SC_Object obj )
{
    struct SC_Array* _obj = (struct SC_Array*)obj;
    struct SC_Array* CloneObj = (struct SC_Array*)SC_Array_AllocInit_NL();
    
    SC_Array_LockerInit( CloneObj );
    
    for ( SC_Index i = 0; i < _obj->__count; i++ ) 
    {
        SC_Array_Insert_NL( CloneObj, SC_IndexLast, SC_CloneObj( SC_Array_GetObject_NL( _obj, i ) ) );
    }
    
    return CloneObj;
}


void SC_Array_freeObj( SC_Object obj )
{
    struct SC_Array* _obj = (struct SC_Array*)obj;
    
    for ( int i = 0; i < _obj->__count; i++ )
    {
        SC_RunTime_Release( _obj->__array[i] );
    }
    
    free( _obj->__array );
    
    pthread_mutex_destroy( &_obj->__locker );
}