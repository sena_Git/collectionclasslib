//
//  SC_Log.c
//  SenaClass
//
//  Created by matsushita sena on 2018/08/26.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#include "SC_Log.h"

#ifdef SC_DEBUGON
extern size_t SC_Format_toStr( char* outPointer, char* buffer, const char* format, va_list ap );

static void SC_DebugSpeedInit_NL( SC_Boolean tagE, SC_Boolean tagD, SC_Boolean tagU );
static void SC_DebugSpeedCheck_NL( size_t tag, const char* func, size_t line, const char* format );

const char tagChara[3] = {'E', 'D', 'U'};

typedef struct{
    struct tm*      __date;
    struct timeval  __nano;
} SC_TimeData;

typedef struct{
    SC_Boolean          __Init;
    SC_Boolean          __UseTag[3];
    SC_TimeData         __InitTime;
    SC_TimeData         __BeforeCheck;
    pthread_mutex_t     __locker;
} SC_SpeedData;

static void SC_Log_LockerInit();
static void SC_Log_Lock();
static void SC_Log_UnLock();

static SC_SpeedData g_SpeedData = {0};

//-----------------------------------public-----------------------------------------------
void SC_DebugSpeedInit( SC_Boolean CheckE, SC_Boolean CheckD, SC_Boolean CheckU )
{
    SC_Log_LockerInit();
    SC_Log_Lock();
    SC_DebugSpeedInit_NL( CheckE, CheckD, CheckU );
    SC_Log_UnLock();
}

void SC_DebugSpeedCheck( size_t tagNum, const char* func, size_t line, const char* format, ... )
{
    va_list ap;
    va_start( ap, format );
    char buf[64] = {0};
    char* string = buf;
    size_t len = SC_Format_toStr( string, buf, format, ap );
    va_end( ap );
    
    SC_Log_Lock();
    SC_DebugSpeedCheck_NL( tagNum, func, line, string );
    SC_Log_UnLock();
    
    if( 64 <= len )
    {
        free( string );
    }
}

//-----------------------------------private-----------------------------------------------
static void SC_DebugSpeedInit_NL( SC_Boolean tagE, SC_Boolean tagD, SC_Boolean tagU )
{
    struct timeval nowNano = {0};
    struct tm* NowDate = {0};
    
    gettimeofday( &nowNano, NULL );
    NowDate = localtime( &nowNano.tv_sec );
    printf( "SC_DebugSpeedStart:  %d/%02d/%02d %02d:%02d:%02d.%06d\n", 
           NowDate->tm_year+1900, NowDate->tm_mon+1, NowDate->tm_mday, 
           NowDate->tm_hour, NowDate->tm_min, NowDate->tm_sec, nowNano.tv_usec );
    
    g_SpeedData.__UseTag[0] = tagE;
    g_SpeedData.__UseTag[1] = tagD;
    g_SpeedData.__UseTag[2] = tagU;
    g_SpeedData.__InitTime.__date = NowDate;
    g_SpeedData.__InitTime.__nano = nowNano;
    g_SpeedData.__BeforeCheck.__date = NowDate;
    g_SpeedData.__BeforeCheck.__nano = nowNano;
}


//-------------------------------------------
static void SC_DebugSpeedCheck_NL( size_t tagNum, const char* func, size_t line, const char* format )
{
    struct timeval nowNano = {0};
    struct tm* NowDate = {0};
    
    SC_UInt64 nowMicro;
    SC_UInt64 befotMicro;
    
    
    gettimeofday( &nowNano, NULL );
    NowDate = localtime( &nowNano.tv_sec );
    
    nowMicro = (NowDate->tm_sec*1000*1000) + nowNano.tv_usec;
    befotMicro = (g_SpeedData.__InitTime.__date->tm_sec*1000*1000) + g_SpeedData.__BeforeCheck.__nano.tv_usec;
    g_SpeedData.__BeforeCheck.__date = NowDate;
    g_SpeedData.__BeforeCheck.__nano = nowNano;
    
    
    if( g_SpeedData.__UseTag[tagNum] == SC_TRUE )
    {        
        printf( "SC_DebugSpeedCheck: %d/%02d/%02d %02d:%02d:%02d.%06d, fromBefo:%llu, Tag(%c), %s/(%lu), %s\n", 
               NowDate->tm_year+1900, NowDate->tm_mon+1, NowDate->tm_mday, 
               NowDate->tm_hour, NowDate->tm_min, NowDate->tm_sec, nowNano.tv_usec, 
               nowMicro - befotMicro, tagChara[tagNum], func, line, format);
    }
}

//-------------------------------------------
static void SC_Log_LockerInit()
{
    pthread_mutex_init( &g_SpeedData.__locker, NULL );
}
//-------------------------------------------
static void SC_Log_Lock()
{
    pthread_mutex_lock( &g_SpeedData.__locker );
}
//-------------------------------------------
static void SC_Log_UnLock()
{
    pthread_mutex_unlock( &g_SpeedData.__locker );
}
#endif
