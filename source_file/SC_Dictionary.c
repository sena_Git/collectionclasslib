//
//  SC_Dictionary.c
//  SenaClass
//
//  Created by matsushita sena on 2018/08/30.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#include "SC_Dictionary.h"
#include "SC_RunTime.h"
#include "SC_Common.h"
#include "SC_Integer.h"
#include "SC_Float.h"
#include "SC_String.h"
#include "SC_Array.h"


struct SC_Dictionary {
    struct SC_RunTime           __runtime;
    
    pthread_mutex_t             __locker;
    
    size_t                      __count;            //格納しているオブジェクトの数
    SC_Object                   __keyArray;             //__memOneBlockSize*__memBlockCountのサイズでリサイズ
    SC_Object                   __objArray;
};


#ifdef SC_TYPECHECK
    static void SC_Dictionary_TypeChecker( SC_TypeID* objId, const char* FuncName);
    #define __Dictionary_TypeChecker( obj ) (SC_Dictionary_TypeChecker( (SC_TypeID*)obj, __func__ ))
#else 
    #define __Dictionary_TypeChecker( ... )
#endif

static SC_Index SC_Dictionary_FindIndex( SC_Object obj, const char* key );

//---------------------------------NoLockFunc---------------------------------------------
SC_Object SC_Dictionary_AllocInit_NL( void );
void SC_Dictionary_Append_NL( SC_Object obj, SC_Object InputObj, const char* key );
void SC_Dictionary_Remove_NL( SC_Object obj, const char* key );
void SC_Dictionary_Replace_NL( SC_Object obj, SC_Object replObj, const char* key );
SC_Boolean SC_Dictionary_Find_NL( SC_Object obj, const char* key );
size_t SC_Dictionary_GetCount_NL( SC_Object obj );
SC_Object SC_Dictionary_GetObject_NL( SC_Object obj, const char* key );
SC_Object SC_Dictionary_CopyObj_NL( SC_Object obj, const char* key );
SC_Object SC_Dictionary_GetKeys_NL( SC_Object obj );

//-------------------------------MutexLocker---------------------------------------------
static void SC_Dictionary_LockerInit( struct SC_Dictionary* obj );
static void SC_Dictionary_Lock( struct SC_Dictionary* obj );
static void SC_Dictionary_UnLock( struct SC_Dictionary* obj );

//--------------------------------CallBackFuncs-------------------------------------------
SC_Object SC_Dictionary_CloneObj( SC_Object obj );
void SC_Dictionary_freeObj( SC_Object obj );
void SC_Dictionary_ToJson( SC_Object obj, SC_Object JsonBuffer );
void SC_Dictionary_LogInfo( SC_Object obj, struct SC_LogBuffer* LogBuffer );
//-----LogInfo----
extern void SC_LogBuffer_AddData( struct SC_LogBuffer* LogBuffer, const char* AddLog );

//--------------------------------RunTime_Alloc-------------------------------------------
extern void* SC_RunTime_AllocInit( SC_TypeID typeId, size_t obj_size );


//--------------------------------Retain&Release------------------------------------------
extern void SC_RunTime_Retain( SC_Object obj );
extern void SC_RunTime_Release( SC_Object obj );

//--------------------------------ForKeyVaArgs--------------------------------------------
extern size_t SC_Format_toStr( char* outPointer, char* buf, const char* format, va_list ap );

//-------------------------------PoolAutoRelease------------------------------------------
extern SC_Object SC_Pool_Autorelease( SC_Object SetObj );



SC_Object SC_Dictionary_AllocInit( void )
{
    SC_Object obj = SC_Dictionary_AllocInit_NL();
    SC_Dictionary_LockerInit( obj );
    
    return obj;
}

SC_Object SC_Dictionary_AllocInitAutorelease( void )
{
    return SC_Pool_Autorelease( SC_Dictionary_AllocInit() );
}

SC_Object SC_Dictionary_AllocInit_NL( void )
{
    struct SC_Dictionary* _obj = SC_RunTime_AllocInit( TypeId_Dictionary, sizeof( struct SC_Dictionary ) );
    _obj->__runtime.__FreeObj = SC_Dictionary_freeObj;
    _obj->__runtime.__CloneObj = SC_Dictionary_CloneObj;
    _obj->__runtime.__ToJson = SC_Dictionary_ToJson;
    _obj->__runtime.__LogInfo = SC_Dictionary_LogInfo;
    _obj->__count  = 0;
    _obj->__keyArray = SC_Array_AllocInit();
    _obj->__objArray = SC_Array_AllocInit();
    
    return _obj;
}



void SC_Dictionary_Append( SC_Object obj, SC_Object InputObj, const char* key )
{
    __Dictionary_TypeChecker( obj );
    
    SC_Dictionary_Lock( obj );
    SC_Dictionary_Append_NL( obj, InputObj, key );
    SC_Dictionary_UnLock( obj );
    
}

void SC_Dictionary_AppendFormat( SC_Object obj, SC_Object InputObj, const char* format, ... )
{
    __Dictionary_TypeChecker( obj );
    va_list ap;
    va_start( ap, format );
    char buf[64] = {0};
    char* keyString = buf;
    size_t keyLen = SC_Format_toStr( keyString, buf, format, ap );
    va_end( ap );
    
    SC_Dictionary_Append( obj, InputObj, keyString );
    
    if( 64 <= keyLen )
    {
        free( keyString );
    }
}

void SC_Dictionary_Append_NL( SC_Object obj, SC_Object InputObj, const char* key )
{
    struct SC_Dictionary* _obj = (struct SC_Dictionary*)obj;
    
    SC_Index index = SC_Dictionary_FindIndex( _obj, key );
    if( index != SC_IndexNone )
    {
        __DebugComent( "Already Registered" );
        return;
    }
    
    SC_Array_Insert( _obj->__keyArray, (SC_Index)_obj->__count, SC_String_AllocInit( key ) );
    SC_Array_Insert( _obj->__objArray, (SC_Index)_obj->__count, InputObj );
    _obj->__count++;
}



void SC_Dictionary_Remove( SC_Object obj, const char* key )
{
    __Dictionary_TypeChecker( obj );
    
    SC_Dictionary_Lock( obj );
    SC_Dictionary_Remove_NL( obj, key );
    SC_Dictionary_UnLock( obj );
}

void SC_Dictionary_RemoveFormat( SC_Object obj, const char* format, ... )
{
    __Dictionary_TypeChecker( obj );
    va_list ap;
    va_start( ap, format );
    char buf[64] = {0};
    char* keyString = buf;
    size_t keyLen = SC_Format_toStr( keyString, buf, format, ap );
    va_end( ap );
    
    SC_Dictionary_Remove( obj, keyString );
    
    if( 64 <= keyLen )
    {
        free( keyString );
    }
}

void SC_Dictionary_Remove_NL( SC_Object obj, const char* key )
{
    struct SC_Dictionary* _obj = (struct SC_Dictionary*)obj;
        
    SC_Index index = SC_Dictionary_FindIndex( _obj, key );
    
    if( index == SC_IndexNone )
    {
        __DebugComent( "Unknown In Dictionary" );
    }else{
        SC_Array_Remove( _obj->__keyArray, index );
        SC_Array_Remove( _obj->__objArray, index );
    }
}



void SC_Dictionary_Replace( SC_Object obj, SC_Object replObj, const char* key )
{
    __Dictionary_TypeChecker( obj );
    
    SC_Dictionary_Lock( obj );
    SC_Dictionary_Replace_NL( obj, replObj, key );
    SC_Dictionary_UnLock( obj );
}

void SC_Dictionary_ReplaceFormat( SC_Object obj, SC_Object replObj, const char* format, ... )
{
    __Dictionary_TypeChecker( obj );
    va_list ap;
    va_start( ap, format );
    char buf[64] = {0};
    char* keyString = buf;
    size_t keyLen = SC_Format_toStr( keyString, buf, format, ap );
    va_end( ap );
    
    SC_Dictionary_Replace( obj, replObj, keyString );
    
    if( 64 <= keyLen )
    {
        free( keyString );
    }
}

void SC_Dictionary_Replace_NL( SC_Object obj, SC_Object replObj, const char* key )
{
    __Dictionary_TypeChecker( obj );
    struct SC_Dictionary* _obj = (struct SC_Dictionary*)obj;
    
    SC_Index index = SC_Dictionary_FindIndex( _obj, key );
    
    if( index == SC_IndexNone )
    {
        __DebugComent( "Unknown In Dictionary" );
    }else{
        SC_Array_Replace( _obj->__objArray, (SC_SInt32)index, replObj );
    }
}



SC_Boolean SC_Dictionary_Find( SC_Object obj, const char* key )
{
    __Dictionary_TypeChecker( obj );
    
    SC_Dictionary_Lock( obj );
    SC_Boolean hit = SC_Dictionary_Find_NL( obj, key );
    SC_Dictionary_UnLock( obj );
    
    return hit;
}

SC_Boolean SC_Dictionary_FindFormat( SC_Object obj, const char* format, ... )
{
    __Dictionary_TypeChecker( obj );
    va_list ap;
    va_start( ap, format );
    char buf[64] = {0};
    char* keyString = buf;
    size_t keyLen = SC_Format_toStr( keyString, buf, format, ap );
    va_end( ap );
    
    SC_Boolean hit = SC_Dictionary_Find( obj, keyString );
    
    if( 64 <= keyLen )
    {
        free( keyString );
    }
    return  hit;
}

SC_Boolean SC_Dictionary_Find_NL( SC_Object obj, const char* key )
{
    __Dictionary_TypeChecker( obj );
	struct SC_Dictionary* _obj = (struct SC_Dictionary*)obj;
	
	SC_Boolean hit;
    SC_Index index = SC_Dictionary_FindIndex( _obj, key );
    
    if( index == SC_IndexNone )
    {
        hit = SC_FALSE;
    }else{
        hit = SC_TRUE;
    }
    
	return hit;
}



size_t SC_Dictionary_GetCount( SC_Object obj )
{
    __Dictionary_TypeChecker( obj );
    
    SC_Dictionary_Lock( obj );
    size_t count = SC_Dictionary_GetCount_NL( obj );
    SC_Dictionary_UnLock( obj );
    
    return count;
}

size_t SC_Dictionary_GetCount_NL( SC_Object obj )
{
    struct SC_Dictionary* _obj = (struct SC_Dictionary*)obj;
    
    return _obj->__count;
}



SC_Object SC_Dictionary_GetObject( SC_Object obj, const char* key )
{
    __Dictionary_TypeChecker( obj );
    
    SC_Dictionary_Lock( obj );
    SC_Object OutputObj = SC_Dictionary_GetObject_NL( obj, key );
    SC_Dictionary_UnLock( obj );
    
    return OutputObj;
}

SC_Object SC_Dictionary_GetObjectFormat( SC_Object obj, const char* format, ... )
{
    __Dictionary_TypeChecker( obj );
    va_list ap;
    va_start( ap, format );
    char buf[64] = {0};
    char* keyString = buf;
    size_t keyLen = SC_Format_toStr( keyString, buf, format, ap );
    va_end( ap );
    
    SC_Object OutputObj = SC_Dictionary_GetObject( obj, keyString );
    
    if( 64 <= keyLen )
    {
        free( keyString );
    }
    return OutputObj;
}

SC_Object SC_Dictionary_GetObject_NL( SC_Object obj, const char* key )
{
    struct SC_Dictionary* _obj = (struct SC_Dictionary*)obj;
    
    SC_Index index = SC_Dictionary_FindIndex( _obj, key );
    if( index == SC_IndexNone )
    {
        __DebugComent( "Unknown In Dictionary" );
        return NULL;
        
    }
    return SC_Array_GetObject( _obj->__objArray, index );
}



SC_Object SC_Dictionary_CopyObj( SC_Object obj, const char* key )
{
    __Dictionary_TypeChecker( obj );
    
    SC_Dictionary_Lock( obj );
    SC_Object CopyObj = SC_Dictionary_CopyObj_NL( obj, key );
    SC_Dictionary_UnLock( obj );
    
    return CopyObj;
}

SC_Object SC_Dictionary_CopyObjAutorelease( SC_Object obj, const char* key )
{
    return SC_Pool_Autorelease( SC_Dictionary_CopyObj( obj, key ) );
}

SC_Object SC_Dictionary_CopyObjFormat( SC_Object obj, const char* format, ... )
{
    __Dictionary_TypeChecker( obj );
    va_list ap;
    va_start( ap, format );
    char buf[64] = {0};
    char* keyString = buf;
    size_t keyLen = SC_Format_toStr( keyString, buf, format, ap );
    va_end( ap );
    
    SC_Object CopyObj = SC_Dictionary_CopyObj( obj, keyString );
    
    if( 64 <= keyLen )
    {
        free( keyString );
    }
    return CopyObj;
}

SC_Object SC_Dictionary_CopyObjFormatAutorelease( SC_Object obj, const char* format, ... )
{
    __Dictionary_TypeChecker( obj );
    va_list ap;
    va_start( ap, format );
    char buf[64] = {0};
    char* keyString = buf;
    size_t keyLen = SC_Format_toStr( keyString, buf, format, ap );
    va_end( ap );
    
    SC_Object CopyObj = SC_Pool_Autorelease( SC_Dictionary_CopyObj( obj, keyString ) );
    
    if( 64 <= keyLen )
    {
        free( keyString );
    }
    return CopyObj;
}

SC_Object SC_Dictionary_CopyObj_NL( SC_Object obj, const char* key )
{
    struct SC_Dictionary* _obj = (struct SC_Dictionary*)obj;
    
    SC_Index index = SC_Dictionary_FindIndex( _obj, key );
    if( index == SC_IndexNone )
    {
        __DebugComent( "Unknown In Dictionary" );
        return NULL;
    }
    
    SC_Object CopyObj = SC_CloneObj( SC_Array_GetObject( obj, index ) );
    SC_RunTime_Release( CopyObj );
    return CopyObj;
}



SC_Object SC_Dictionary_GetKeys( SC_Object obj )
{
    __Dictionary_TypeChecker( obj );
    
    SC_Dictionary_Lock( obj );
    SC_Object keys = SC_Dictionary_GetKeys_NL( obj );
    SC_Dictionary_UnLock( obj );
    
    return keys;
}

SC_Object SC_Dictionary_GetKeysAutorelease( SC_Object obj )
{
    return SC_Pool_Autorelease( SC_Dictionary_GetKeys( obj ) );
}

SC_Object SC_Dictionary_GetKeys_NL( SC_Object obj )
{
	struct SC_Dictionary* _obj = (struct SC_Dictionary*)obj;
	
	return SC_CloneObj( _obj->__keyArray );
}



//-----------------------------------private-----------------------------------------------
#ifdef SC_TYPECHECK
static void SC_Dictionary_TypeChecker( SC_TypeID* objId, const char* FuncName )
{
    if( objId == NULL )
    {
        __DebugTypeEr( FuncName );
    }
    
    if( *objId != TypeId_Dictionary )
    {
        __DebugTypeEr( FuncName );
    }
}
#endif


static SC_Index SC_Dictionary_FindIndex( SC_Object obj, const char* key )
{
    struct SC_Dictionary* _obj = (struct SC_Dictionary*)obj;
    
    SC_Index index = SC_IndexNone;
    for ( SC_Index i = 0; i < _obj->__count; i++ )
    {
        if( SC_String_Compare( SC_Array_GetObject( _obj->__keyArray, i ), key ) == SC_TRUE )
        {
            index = i;
            break;
        }
    }
    
    return index;
}


static void SC_Dictionary_LockerInit( struct SC_Dictionary* obj )
{
    pthread_mutex_init( &obj->__locker, NULL );
}

static void SC_Dictionary_Lock( struct SC_Dictionary* obj )
{
    pthread_mutex_lock( &obj->__locker );
}

static void SC_Dictionary_UnLock( struct SC_Dictionary* obj )
{
    pthread_mutex_unlock( &obj->__locker );
}



//----------------CallBacks---------------------
SC_Object SC_Dictionary_CloneObj( SC_Object obj )
{
    struct SC_Dictionary* _obj = (struct SC_Dictionary*)obj;
    
    struct SC_Dictionary* CloneObj = (struct SC_Dictionary*)SC_Dictionary_AllocInit_NL();
    CloneObj->__count = _obj->__count;
    CloneObj->__keyArray = SC_CloneObj( _obj->__keyArray );
    CloneObj->__objArray = SC_CloneObj( _obj->__objArray );
    
    return CloneObj;
}


void SC_Dictionary_LogInfo( SC_Object obj, struct SC_LogBuffer* LogBuffer )
{
    struct SC_Dictionary* _obj = (struct SC_Dictionary*)obj;
    
    char msg[128] = {0};
    sprintf( msg, "Calledfunc(%s) :: count(%zu) / ", __func__, _obj->__count );
    
    SC_LogBuffer_AddData( LogBuffer, msg );
}


void SC_Dictionary_ToJson( SC_Object obj, SC_Object JsonBuffer )
{
    struct SC_Dictionary* _obj = (struct SC_Dictionary*)obj;
        
    SC_String_Insert( JsonBuffer, SC_IndexLast, "{" );
        
    for (int i = 0; i < _obj->__count; i++) 
    {
        struct SC_RunTime* tmpObj = (struct SC_RunTime*)SC_Array_GetObject( _obj->__objArray, i );
        
        if( 0<i )
        {
            SC_String_Insert( JsonBuffer, SC_IndexLast, "," );
        }
        
        
        SC_String_Insert( JsonBuffer, SC_IndexLast, "\"" );
        SC_String_InsertObj( JsonBuffer, SC_IndexLast, SC_Array_GetObject( _obj->__keyArray, i ) );
        SC_String_Insert( JsonBuffer, SC_IndexLast, "\":" );
        
        tmpObj->__ToJson( tmpObj, JsonBuffer );
    }
    
    SC_String_Insert( JsonBuffer, SC_IndexLast, "}");
}


void SC_Dictionary_freeObj( SC_Object obj )
{
    struct SC_Dictionary* _obj = (struct SC_Dictionary*)obj;
    
    SC_RunTime_Release( _obj->__keyArray );
    SC_RunTime_Release( _obj->__objArray );
    
    pthread_mutex_destroy( &_obj->__locker );
}
