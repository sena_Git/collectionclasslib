//
//  SC_Dictionary.h
//  SenaClass
//
//  Created by matsushita sena on 2018/08/30.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#ifndef SC_Dictionary_h
#define SC_Dictionary_h


#include "SC_Type.h"

SC_Object SC_Dictionary_AllocInit( void );
SC_Object SC_Dictionary_AllocInitAutorelease( void );

void SC_Dictionary_Append( SC_Object obj, SC_Object InputObj, const char* key );
void SC_Dictionary_AppendFormat( SC_Object obj, SC_Object InputObj, const char* format, ... );

void SC_Dictionary_Remove( SC_Object obj, const char* key );
void SC_Dictionary_RemoveFormat( SC_Object obj, const char* format, ... );

void SC_Dictionary_Replace( SC_Object obj, SC_Object replObj, const char* key );
void SC_Dictionary_ReplaceFormat( SC_Object obj, SC_Object replObj, const char* format, ... );

SC_Boolean SC_Dictionary_Find( SC_Object obj, const char* key );
SC_Boolean SC_Dictionary_FindFormat( SC_Object obj, const char* format, ... );

size_t SC_Dictionary_GetCount( SC_Object obj );

SC_Object SC_Dictionary_GetObject( SC_Object obj, const char* key );
SC_Object SC_Dictionary_GetObjectFormat( SC_Object obj, const char* format, ... );

SC_Object SC_Dictionary_CopyObj( SC_Object obj, const char* key );
SC_Object SC_Dictionary_CopyObjAutorelease( SC_Object obj, const char* key );
SC_Object SC_Dictionary_CopyObjFormat( SC_Object obj, const char* format, ... );
SC_Object SC_Dictionary_CopyObjFormatAutorelease( SC_Object obj, const char* format, ... );

SC_Object SC_Dictionary_GetKeys( SC_Object obj );
SC_Object SC_Dictionary_GetKeysAutorelease( SC_Object obj );

#endif /* SC_Dictionary_h */
