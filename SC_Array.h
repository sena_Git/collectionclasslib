#ifndef SC_Array_h
#define SC_Array_h

#include "SC_Type.h"

SC_Object SC_Array_AllocInit( void );
SC_Object SC_Array_AllocInitAutorelease( void );

void SC_Array_Insert( SC_Object obj, SC_Index index, SC_Object InputObj );

void SC_Array_Replace( SC_Object obj, SC_Index index, SC_Object repl );

void SC_Array_Remove( SC_Object obj, SC_Index index );

SC_Index SC_Array_IndexFind( SC_Object obj, SC_Object sarch );

size_t SC_Array_GetCount( SC_Object obj );

SC_Object SC_Array_GetObject( SC_Object obj, SC_Index index );

SC_Object SC_Array_CopyObj( SC_Object obj, SC_Index index );
SC_Object SC_Array_CopyObjAutorelease( SC_Object obj, SC_Index index );


#endif /* SC_Array_h */
