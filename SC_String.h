//
//  SC_String.h
//  CollectionClass
//
//  Created by matsushita sena on 2018/08/19.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#ifndef SC_String_h
#define SC_String_h

#include "SC_Type.h"


SC_Object SC_String_AllocInit( const char* string );
SC_Object SC_String_AllocInitAutorelease( const char* string );
SC_Object SC_String_AllocInitFormat( const char* format, ... );
SC_Object SC_String_AllocInitFormatAutorelease( const char* format, ... );

void SC_String_Insert( SC_Object obj, SC_Index index, const char* string );
void SC_String_InsertFormat( SC_Object obj, SC_Index index, const char* format, ... );

void SC_String_InsertObj( SC_Object obj, SC_Index index, SC_Object inputObj );

void SC_String_Remove( SC_Object obj, SC_Index fromIndex, size_t size );

void SC_String_Replace( SC_Object obj, const char* target, const char* repl );

SC_Boolean SC_String_Compare( SC_Object obj, const char* sarch );
SC_Boolean SC_String_CompareFormat( SC_Object obj, const char* format, ... );

SC_Index SC_String_IndexFind( SC_Object obj, const char* sarch );
SC_Index SC_String_IndexFindFormat( SC_Object obj, const char* format, ... );

char SC_String_GetAtIndex( SC_Object obj, SC_Index index );

void SC_String_OutPut(  const char* AftDisp, SC_Object obj, const char* BfoDisp );

size_t SC_String_GetLength( SC_Object obj );

void SC_String_CopyStr( SC_Object obj, char* str, size_t getLen );

const char* SC_String_GetCstr( SC_Object obj );


#endif /* SC_String_h */
