//
//  SC_RunTime.c
//  CollectionClass
//
//  Created by matsushita sena on 2018/08/19.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#include "SC_RunTime.h"

#include "SC_Common.h"


#ifdef SC_TYPECHECK
    static void SC_RunTime_TypeChecker( SC_TypeID* objId, const char* FuncName);
    #define __RunTime_TypeChecker( obj ) (SC_RunTime_TypeChecker( obj, __func__ ))
#else 
    #define __RunTime_TypeChecker(...)
#endif


static inline void SC_RunTime_Lock( struct SC_RunTime* obj );
static inline void SC_RunTime_UnLock( struct SC_RunTime* obj );

//----------------------------------LogBuffer--------------------------------------------- 
void SC_LogBuffer_Init( struct SC_LogBuffer* LogBuffer );
void SC_LogBuffer_AddData( struct SC_LogBuffer* LogBuffer, const char* AddLog );
void SC_LogBuffer_Free( struct SC_LogBuffer* LogBuffer );

//---------------------------------String-Func--------------------------------------------
extern SC_Object SC_String_AllocInit( const char* );

//-------------------------------PoolAutoRelease------------------------------------------
extern SC_Object SC_Pool_Autorelease( SC_Object SetObj );



//-----------------------------------public-----------------------------------------------
SC_TypeID SC_GetTypeId( SC_Object obj )
{
    __RunTime_TypeChecker( obj );
    struct SC_RunTime* _obj = (struct SC_RunTime*)obj;
    
    return _obj->__typeId;
}


//-------------------------------------------
SC_Object SC_CloneObj( SC_Object obj )
{
    __RunTime_TypeChecker( obj );
    struct SC_RunTime* _obj = (struct SC_RunTime*)obj;
    
    struct SC_RunTime* CloneObj = (struct SC_RunTime*)_obj->__CloneObj( obj );
    
    SC_RunTime_Lock( _obj );
    CloneObj->__retain = _obj->__retain;
    CloneObj->__release = _obj->__release;
    SC_RunTime_UnLock( _obj );
    
    return (SC_Object)CloneObj;
}

SC_Object SC_CloneObjAutorelease( SC_Object obj )
{
    return SC_Pool_Autorelease( SC_CloneObj( obj ) );
}


//-------------------------------------------
void SC_LogInfo( SC_Object obj )
{
    __RunTime_TypeChecker( obj );
    struct SC_RunTime* _obj = (struct SC_RunTime*)obj;
    
    SC_RunTime_Lock( _obj );
    SC_UInt32 ObjRetain = _obj->__retain;
    SC_UInt32 ObjReleaas = _obj->__release;
    SC_RunTime_UnLock( _obj );
  
    struct SC_LogBuffer LogBuffer; 
    SC_LogBuffer_Init( &LogBuffer );
    
    _obj->__LogInfo( obj, &LogBuffer );
        
    printf( "%sTypeId(%d) / Retain(%d) / Releas(%d)\n", LogBuffer.__Log, _obj->__typeId, ObjRetain, ObjReleaas);
    
    
    SC_LogBuffer_Free( &LogBuffer );
}



//-------------------------------------------
SC_Object SC_ToJson( SC_Object obj )
{
    __RunTime_TypeChecker( obj );
    struct SC_RunTime* _obj = (struct SC_RunTime*)obj;
    
    SC_RunTime_Lock( _obj );
    SC_Object JsonBuffer = SC_String_AllocInit("");
    _obj->__ToJson( obj, JsonBuffer );
    SC_RunTime_UnLock( _obj );  
    
    return JsonBuffer;
}

SC_Object SC_ToJsonAutorelease( SC_Object obj )
{
    return SC_Pool_Autorelease( SC_ToJson( obj ) );
}




//-----------------------------------OnlySC-----------------------------------------------
void* SC_RunTime_AllocInit( SC_TypeID typeId, size_t obj_size )
{
    struct SC_RunTime* _obj = malloc( obj_size );;
    if (_obj == NULL) 
    {
        __ABRTSTOP( "memory reserve miss" );
    }
    
    memset( _obj, 0x00, obj_size );
    _obj->__typeId = typeId;
    pthread_mutex_init( &_obj->__locker, NULL );
    return _obj;
}



//-------------------------------------------
void SC_RunTime_Retain( SC_Object obj )
{
    if( obj == NULL  )
    {
        __DebugComent( "Object Is NULL" );
        return;
    }
    
    __RunTime_TypeChecker( obj );
    struct SC_RunTime* _obj = (struct SC_RunTime*)obj;
    
    SC_RunTime_Lock( _obj );
    _obj->__retain += 1;
    SC_RunTime_UnLock( _obj );
}


//-------------------------------------------
void SC_RunTime_Release( SC_Object obj )
{
    if( obj == NULL  )
    {
        __DebugComent( "Object Is NULL" );
        return;
    }
    
    __RunTime_TypeChecker( obj );
    struct SC_RunTime* _obj = (struct SC_RunTime*)obj;
    
    SC_Boolean isFree = SC_FALSE;
    
    
    SC_RunTime_Lock( _obj );
    _obj->__release += 1;
    if( _obj->__retain <= _obj->__release )
    {
        isFree = SC_TRUE;
    }
    SC_RunTime_UnLock( _obj );
    
    
    if( isFree )
    {
        _obj->__FreeObj( _obj );
         
        _obj->__typeId = 0;
        pthread_mutex_destroy( &_obj->__locker );
        free( _obj );
    }
}



//-----------------------------------private-----------------------------------------------
#ifdef SC_TYPECHECK
static void SC_RunTime_TypeChecker( SC_TypeID* objId, const char* FuncName)
{
    if( objId == NULL )
    {
        __DebugTypeEr( FuncName );
    }
    
    if( (*objId >> 16) != (SC_TypeID)'SC__' >> 16 )
    {
        __DebugTypeEr( FuncName );
    }
}
#endif

//-------------------------------------------
static inline void SC_RunTime_Lock( struct SC_RunTime* obj )
{
    pthread_mutex_lock( &obj->__locker );
}


//-------------------------------------------
static inline void SC_RunTime_UnLock( struct SC_RunTime* obj )
{
    pthread_mutex_unlock( &obj->__locker );
}


//-----------------------------------LogBuffer-----------------------------------------------
void SC_LogBuffer_Init( struct SC_LogBuffer* LogBuffer )
{
    struct SC_LogBuffer* _LogBuffer = (struct SC_LogBuffer*)LogBuffer;
    _LogBuffer->__LogSize = 0;
    _LogBuffer->__Log = NULL;
}


//-------------------------------------------
void SC_LogBuffer_AddData( struct SC_LogBuffer* LogBuffer, const char* AddLog )
{
    struct SC_LogBuffer* _LogBuffer = (struct SC_LogBuffer*)LogBuffer;
    
    size_t AddLen = 0;
    AddLen = strlen( AddLog )+1;
    
    char* tmp = realloc( _LogBuffer->__Log, sizeof( char )*(_LogBuffer->__LogSize+AddLen) );
    if ( tmp == NULL ) 
    {
        free( _LogBuffer->__Log );
        __ABRTSTOP( "memory reserve miss" );
    }
    _LogBuffer->__Log = tmp;
    
    memcpy( &(_LogBuffer->__Log[_LogBuffer->__LogSize]), AddLog, sizeof( char )*AddLen );
    
    _LogBuffer->__Log = tmp;
    _LogBuffer->__LogSize += AddLen;

}


//-------------------------------------------
void SC_LogBuffer_Free( struct SC_LogBuffer* LogBuffer )
{
    struct SC_LogBuffer* _LogBuffer = (struct SC_LogBuffer*)LogBuffer;
    
    free( _LogBuffer->__Log );
}