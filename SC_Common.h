//
//  SC_Common.h
//  SenaClass
//
//  Created by matsushita sena on 2019/01/20.
//  Copyright © 2019年 matsushita sena. All rights reserved.
//

#ifndef SC_Common_h
#define SC_Common_h

#include "SC_Type.h"

//if you use SC, you must call this.
void SC_Initializer( void );

//if you finished using SC, you must call this.
void SC_Finalizer( void );

SC_TypeID SC_GetTypeId( SC_Object obj );

SC_Object SC_CloneObj( SC_Object obj );
SC_Object SC_CloneObjAutorelease( SC_Object obj );

SC_Object SC_ToJson( SC_Object obj );
SC_Object SC_ToJsonAutorelease( SC_Object obj );

void SC_LogInfo( SC_Object obj );




#endif /* SC_Common_h */
