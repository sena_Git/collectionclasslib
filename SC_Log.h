//
//  SC_Log.h
//  SenaClass
//
//  Created by matsushita sena on 2018/08/26.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#ifndef SC_Log_h
#define SC_Log_h

#include "SC_Type.h"

#ifdef SC_DEBUGON
    void SC_DebugSpeedInit( SC_Boolean CheckE, SC_Boolean CheckD, SC_Boolean CheckU );
    void SC_DebugSpeedCheck( size_t tagNum, const char* func, size_t line, const char* format, ... );
    #define __DebugSpeedInit( tagE, tagD, tagU )    SC_DebugSpeedInit( tagE, tagD, tagU )
    #define __DebugSpeedCheckE( fmt, ... )          SC_DebugSpeedChck( 0, __func__, __LINE__, fmt, __VA_ARGS__ )
    #define __DebugSpeedCheckD( fmt, ... )          SC_DebugSpeedChck( 1, __func__, __LINE__, fmt, __VA_ARGS__ )
    #define __DebugSpeedCheckU( fmt, ... )          SC_DebugSpeedChck( 2, __func__, __LINE__, fmt, __VA_ARGS__ )
#else
    #define __DebugSpeedInit( ... )
    #define __DebugSpeedCheckE( ... )
    #define __DebugSpeedCheckD( ... )
    #define __DebugSpeedCheckU( ... )
#endif


#endif /* SC_Log_h */
