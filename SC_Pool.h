//
//  SC_Pool.h
//  SenaClass
//
//  Created by matsushita sena on 2018/12/22.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#ifndef SC_Pool_h
#define SC_Pool_h

#include "SC_Type.h"

void SC_Pool_Alloc( void );

void SC_Pool_Release( void );

void SC_Pool_CurrentUp( SC_Object obj );

#endif /* SC_Pool_h */
