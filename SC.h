//
//  CollectionClass.h
//  CollectionClass
//
//  Created by matsushita sena on 2018/08/14.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#ifndef SC_h
#define SC_h

#include "SC_Type.h"
//Log
#include "SC_Log.h"
//SC Common
#include "SC_Common.h"
//Type Object
#include "SC_Integer.h"
#include "SC_Float.h"
#include "SC_String.h"
#include "SC_Binary.h"
//Collection
#include "SC_Array.h"
#include "SC_Dictionary.h"
//Memory Pool
#include "SC_Pool.h"

#endif /* SC_h */