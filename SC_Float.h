//
//  SC_Float32.h
//  SenaClass
//
//  Created by matsushita sena on 2018/10/06.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#ifndef SC_RealNum_h
#define SC_RealNum_h

#include "SC_Type.h"

SC_Object SC_Float_AllocInit( SC_Float64 value );
SC_Object SC_Float_AllocInitAutorelease( SC_Float64 value );

SC_Float64 SC_Float_GetValue( SC_Object obj );

void SC_Float_ChangeValue( SC_Object obj, SC_Float64 ToValue );




#endif /* SC_RealNum_h */
