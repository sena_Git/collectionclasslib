//
//  SC_Float32
//  SenaClass
//
//  Created by matsushita sena on 2018/10/06.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#include "SC_Float.h"
#include "SC_RunTime.h"


struct SC_Float64{
    struct SC_RunTime          __runtime;
    
    pthread_mutex_t     __locker;
    
    SC_Float64          __value;
};


#ifdef SC_TYPECHECK
    static void SC_Float_TypeChecker( SC_TypeID* objId, const char* FuncName );
    #define __Float_TypeChecker( obj ) (SC_Float_TypeChecker( (SC_TypeID*)obj, __func__ ))
#else 
    #define __Float_TypeChecker( ... )
#endif


//---------------------------------NoLockFunc---------------------------------------------
SC_Object SC_Float_AllocInit_NL( SC_Float64 value );
SC_Float64 SC_Float_GetValue_NL( SC_Object obj );
void SC_Float_ChangeValue_NL( SC_Object obj, SC_Float64 ToValue );

//-------------------------------MutexLocker---------------------------------------------
static void SC_Float_LockerInit( struct SC_Float64* obj );
static void SC_Float_Lock( struct SC_Float64* obj );
static void SC_Float_UnLock( struct SC_Float64* obj );

//--------------------------------CallBackFuncs-------------------------------------------
SC_Object SC_Float_CloneObj( SC_Object obj );
void SC_Float_LogInfo( SC_Object obj, struct SC_LogBuffer* LogBuffer );
void SC_Float_ToJson( SC_Object obj, SC_Object JsonBuffer );
void SC_Float_freeObj( SC_Object obj );
//-----LogInfo----
extern void SC_LogBuffer_AddData( struct SC_LogBuffer* LogBuffer, const char* AddLog );

//--------------------------------RunTime_Alloc-------------------------------------------
extern void* SC_RunTime_AllocInit( SC_TypeID typeId, size_t obj_size );

//--------------------------------String_Alloc---------------------------------------------
extern SC_Object SC_String_AllocInit( const char* format, ... );
extern void SC_String_Insert( SC_Object obj, SC_SInt32 index, const char* format, ... );

//-------------------------------PoolAutoRelease------------------------------------------
extern SC_Object SC_Pool_Autorelease( SC_Object SetObj );



SC_Object SC_Float_AllocInit( SC_Float64 value )
{
    
    struct SC_Float64* obj = SC_Float_AllocInit_NL( value );
    SC_Float_LockerInit( obj );
    return (SC_Object)obj;
}

SC_Object SC_Float_AllocInitAutorelease( SC_Float64 value )
{
    return SC_Pool_Autorelease( SC_Float_AllocInit( value ) );
}

SC_Object SC_Float_AllocInit_NL( SC_Float64 value )
{
    struct SC_Float64* _obj = SC_RunTime_AllocInit( TypeId_Float, sizeof( struct SC_Float64 ) );
    _obj->__runtime.__ToJson = SC_Float_ToJson;
    _obj->__runtime.__CloneObj = SC_Float_CloneObj;
    _obj->__runtime.__FreeObj = SC_Float_freeObj;
    _obj->__runtime.__LogInfo = SC_Float_LogInfo;
    _obj->__value = value;
    
    return _obj;
}



SC_Float64 SC_Float_GetValue( SC_Object obj )
{
    __Float_TypeChecker( obj );
    
    SC_Float_Lock( obj );
    SC_Float64 _value = SC_Float_GetValue_NL( obj );
    SC_Float_UnLock( obj );
    
    return  _value;
}

SC_Float64 SC_Float_GetValue_NL( SC_Object obj )
{
    struct SC_Float64* _obj = (struct SC_Float64*)obj;
    
    return  _obj->__value;
}



void SC_Float_ChangeValue( SC_Object obj, SC_Float64 ToValue )
{
    __Float_TypeChecker( obj );
    
    SC_Float_Lock( obj );
    SC_Float_ChangeValue_NL( obj, ToValue );
    SC_Float_UnLock( obj );
}

void SC_Float_ChangeValue_NL( SC_Object obj, SC_Float64 ToValue )
{
    struct SC_Float64* _obj = (struct SC_Float64*)obj;
    
    _obj->__value = ToValue;
}



//-----------------------------------private-----------------------------------------------
#ifdef SC_TYPECHECK
static void SC_Float_TypeChecker( SC_TypeID* objId, const char* FuncName )
{
    if( objId == NULL )
    {
        __DebugTypeEr( FuncName );
    }
    
    if( *objId != TypeId_Float )
    {
        __DebugTypeEr( FuncName );
    }
}
#endif

static void SC_Float_LockerInit( struct SC_Float64* obj )
{
    pthread_mutex_init( &obj->__locker, NULL );
}
static void SC_Float_Lock( struct SC_Float64* obj )
{
    pthread_mutex_lock( &obj->__locker );
}
static void SC_Float_UnLock( struct SC_Float64* obj )
{
    pthread_mutex_unlock( &obj->__locker );
}


//----------------CallBacks---------------------
void SC_Float_LogInfo( SC_Object obj, struct SC_LogBuffer* LogBuffer )
{
    char msg[64] = {0};
    sprintf( msg, "Calledfunc(%s) :: ", __func__ );
    
    SC_LogBuffer_AddData( LogBuffer, msg );
}

SC_Object SC_Float_CloneObj( SC_Object obj )
{
    struct SC_Float64* _obj = (struct SC_Float64*)obj;
    return SC_Float_AllocInit_NL( _obj->__value);
}

void SC_Float_freeObj( SC_Object obj )
{
    struct SC_Float64* _obj = (struct SC_Float64*)obj;
    pthread_mutex_destroy( &_obj->__locker );
}

void SC_Float_ToJson( SC_Object obj, SC_Object JsonBuffer )
{
    struct SC_Float64* _obj = (struct SC_Float64*)obj;
    
    char tmp[20] = {0};
    
    sprintf(tmp, "%lf", _obj->__value );
    
    SC_String_Insert( JsonBuffer, SC_IndexLast, tmp );
}