//
//  SC_Integer.c
//  CollectionClass
//
//  Created by matsushita sena on 2018/08/19.
//  Copyright © 2018年 matsushita sena. All rights reserved.
//

#include "SC_Integer.h"
#include "SC_RunTime.h"

struct SC_IntegerS64{
    struct SC_RunTime   __runtime;
    
    pthread_mutex_t     __locker;
    
    SC_SInt64           __value;
};

#ifdef SC_TYPECHECK
    static void SC_Integer_TypeChecker( SC_TypeID* objId, const char* FuncName );
    #define __Integer_TypeChecker( obj ) (SC_Integer_TypeChecker( (SC_TypeID*)obj, __func__ ))
#else 
    #define __Integer_TypeChecker( ... )
#endif

//---------------------------------NoLockFunc---------------------------------------------
SC_Object SC_Integer_AllocInit_NL( SC_SInt64 value );
SC_SInt64 SC_Integer_GetValue_NL( SC_Object obj );
void SC_Integer_ChangeValue_NL( SC_Object obj, SC_SInt64 Tovalue );

//-------------------------------MutexLocker---------------------------------------------
static void SC_Integer_LockerInit( struct SC_IntegerS64* obj );
static void SC_Integer_Lock( struct SC_IntegerS64* obj );
static void SC_Integer_UnLock( struct SC_IntegerS64* obj );

//--------------------------------CallBackFuncs-------------------------------------------
SC_Object SC_Integer_CloneObj( SC_Object obj );
void SC_Integer_LogInfo( SC_Object obj, struct SC_LogBuffer* LogDeta );
void SC_Integer_ToJson( SC_Object obj, SC_Object JsonBuffer );
void SC_Integer_freeObj( SC_Object obj );
//-----LogInfo----
extern void SC_LogBuffer_AddData( struct SC_LogBuffer* LogBuffer, const char* AddLog );

//--------------------------------RunTime_Alloc-------------------------------------------
extern void* SC_RunTime_AllocInit( SC_TypeID typeId, size_t obj_size );

//--------------------------------String_Alloc---------------------------------------------
extern SC_Object SC_String_AllocInit( const char* format, ... );
extern void SC_String_Insert( SC_Object obj, SC_SInt32 index, const char* format, ... );

//-------------------------------PoolAutoRelease------------------------------------------
extern SC_Object SC_Pool_Autorelease( SC_Object SetObj );



SC_Object SC_Integer_AllocInit( SC_SInt64 value )
{
    struct SC_IntegerS64* obj = SC_Integer_AllocInit_NL( value );
    SC_Integer_LockerInit( obj );
    
    return (SC_Object*)obj;
}

SC_Object SC_Integer_AllocInitAutorelease( SC_SInt64 value )
{
    return SC_Pool_Autorelease( SC_Integer_AllocInit( value ) );
}

SC_Object SC_Integer_AllocInit_NL( SC_SInt64 value )
{
    struct SC_IntegerS64* _obj = SC_RunTime_AllocInit( TypeId_Integer, sizeof( struct SC_IntegerS64 ) );
    _obj->__runtime.__ToJson = SC_Integer_ToJson;
    _obj->__runtime.__CloneObj = SC_Integer_CloneObj;
    _obj->__runtime.__FreeObj = SC_Integer_freeObj;
    _obj->__runtime.__LogInfo = SC_Integer_LogInfo;
    _obj->__value  = value;
    
    return _obj;
}



SC_SInt64 SC_Integer_GetValue( SC_Object obj )
{
    __Integer_TypeChecker( obj );
    SC_Integer_Lock( obj );
    SC_SInt64 value = SC_Integer_GetValue_NL( obj );
    SC_Integer_UnLock( obj );
    
    return  value;
}

SC_SInt64 SC_Integer_GetValue_NL( SC_Object obj )
{
    struct SC_IntegerS64* _obj = (struct SC_IntegerS64*)obj;
    
    return  _obj->__value;
}



void SC_Integer_ChangeValue( SC_Object obj, SC_SInt64 ToValue )
{
    __Integer_TypeChecker( obj );
    SC_Integer_Lock( obj );
    SC_Integer_ChangeValue_NL( obj, ToValue );
    SC_Integer_UnLock( obj );
}

void SC_Integer_ChangeValue_NL( SC_Object obj, SC_SInt64 ToValue )
{
    struct SC_IntegerS64* _obj = (struct SC_IntegerS64*)obj;
    
    _obj->__value  = ToValue;
}



//-----------------------------------private-----------------------------------------------
#ifdef SC_TYPECHECK
static void SC_Integer_TypeChecker( SC_TypeID* objId, const char* FuncName )
{
    if( objId == NULL )
    {
        __DebugTypeEr( FuncName );
    }
    
    if( *objId != TypeId_Integer )
    {
        __DebugTypeEr( FuncName );
    }
    
}
#endif

static void SC_Integer_LockerInit( struct SC_IntegerS64* obj )
{
    pthread_mutex_init( &obj->__locker, NULL );
}
static void SC_Integer_Lock( struct SC_IntegerS64* obj )
{
    pthread_mutex_lock( &obj->__locker );
}
static void SC_Integer_UnLock( struct SC_IntegerS64* obj )
{
    pthread_mutex_unlock( &obj->__locker );
}


//----------------CallBacks---------------------
void SC_Integer_LogInfo( SC_Object obj, struct SC_LogBuffer* LogDeta )
{    
    char msg[64] = {0};
    sprintf( msg, "Calledfunc(%s) :: ", __func__ );
    
    SC_LogBuffer_AddData( LogDeta, msg );
}

SC_Object SC_Integer_CloneObj( SC_Object obj )
{
    struct SC_IntegerS64* _obj = (struct SC_IntegerS64*)obj;
    return SC_Integer_AllocInit_NL( _obj->__value );;
}

void SC_Integer_freeObj( SC_Object obj )
{
    struct SC_IntegerS64* _obj = (struct SC_IntegerS64*)obj;
    pthread_mutex_destroy( &_obj->__locker );
}

void SC_Integer_ToJson( SC_Object obj, SC_Object JsonBuffer )
{
    struct SC_IntegerS64* _obj = (struct SC_IntegerS64*)obj;
        
    char tmp[20] = {0};
    
    sprintf(tmp, "%lld", _obj->__value );
    
    SC_String_Insert( JsonBuffer, SC_IndexLast, tmp );
}

