#ifndef SC_Type_h
#define SC_Type_h

#include <stdio.h>
#include <stdarg.h>
#include <memory.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>

// Debug
#define SC_DEBUGON

//TypeChecker
#define SC_TYPECHECK

//one malloc byte
#define SC_MALLOCSIZE   (4096)

//sig
#define SC_ERSIG           (SIGUSR1) //Use raise's tag "SIGUSER1" in SC

//You can order last index in Array or String etc..
#define SC_IndexLast        (-1)
#define SC_IndexNone        (-10)

#define SC_TRUE            (1)
#define SC_FALSE           (0)

#define TypeId_Pool             ('SC_P')
#define TypeId_Dictionary       ('SC_D')
#define TypeId_Array            ('SC_A')
#define TypeId_String           ('SC_S')
#define TypeId_Integer          ('SC_I')
#define TypeId_Float            ('SC_F')
#define TypeId_Binary           ('SC_B')


#ifdef SC_DEBUGON
    #define __DebugTypeEr( FuncName )   printf("%s/\"Wrong Type or Not SC_Object\"\n", FuncName);raise(SIGABRT)
    #define __DebugComent( fmt )        printf("%s:%d/\"%s\"\n",__func__,__LINE__,fmt)
    #define __ABRTSTOP( fmt )           printf("%s:%d/\"%s\"\n",__func__,__LINE__,fmt);raise(SIGABRT) //fflush(stdout)...maybe Need
#else
    #define __DebugTypeEr( FuncName )   printf("%s/\"Wrong Type or Not SC_Object\"\n", FuncName)
    #define __DebugComent( ... )   
    #define __ABRTSTOP( fmt )           printf("%s:%d/\"%s\"\n",__func__,__LINE__,fmt)
#endif  /* su_DEBUGON */

typedef unsigned short          SC_Boolean;
typedef unsigned char           SC_UInt8;
typedef signed char             SC_SInt8;
typedef unsigned short          SC_UInt16;
typedef signed short            SC_SInt16;
typedef unsigned int            SC_UInt32;
typedef signed int              SC_SInt32;
typedef unsigned long long      SC_UInt64;
typedef signed long long        SC_SInt64;
//typedef UInt32                  SC_Reasult;
typedef float                   SC_Float32;
typedef double                  SC_Float64;
typedef unsigned int            SC_TypeID;
typedef signed int              SC_Index;
//typedef unsigned char *         SC_StringPtr;
//typedef const unsigned char *   SC_ConstStringPtr;
//typedef char                    SC_SC_str32[32];
//typedef char                    SC_Str64[64];
//typedef char                    SC_Str127[128];
//typedef char                    SC_Str255[256];
//typedef const unsigned char *   ConstStr255Param;
typedef void*                   SC_Object;

#endif /* SC_Type_h */
